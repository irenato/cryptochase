<?php

if (in_category('generic-promo')) {
    get_template_part('templates/generic', 'promo');
} elseif (in_category('standard-promo')) {
    get_template_part('templates/promo');    
}