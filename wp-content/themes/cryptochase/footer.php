    <?php if (is_page('promo')) {
        
    } else { ?>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p class="copyright">CryptoChase Copyright © 2019. </p>
                    </div>
                </div>
            </div>
        </footer>
    <?php } ?>

    <div class="modal fade sign-up-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button class="close icon-close" type="button" data-dismiss="modal" aria-hidden="true"></button>
                <div class="sign-up">
                    <h3>Register for Free</h3>
                    <p>Start your own campaign and fund your dreams. Simple as this.</p>
                    <form method="post" id="register" class="ajax-auth">
                        <div class="input">
                            <label>Username<sup>*</sup></label>
                            <input class="username" type="text" name="username" autocomplete="off">
                        </div>
                        <div class="input">
                            <label>Email<sup>*</sup></label>
                            <input type="email" name="email" autocomplete="off">
                        </div>
                        <div class="input">
                            <input type="email" placeholder="Confirm email *" name="email" autocomplete="off">
                        </div>
                        <div class="input password">
                            <label>Password <sup>*</sup></label>
                            <input type="password" name="password" autocomplete="off"><i class="icon-eye"></i>
                        </div>
                        <div class="input password">
                            <input type="password" placeholder="Confirm Password *" name="password" autocomplete="off"><i class="icon-eye"></i>
                        </div>
                        <div class="checkbox">
                            <input id="terms-privace" type="checkbox">
                            <label for="terms-privace">I agree with <a href="#">Terms and Conditions </a>& <a href="#">Privacy Policy </a>of the service</label>
                        </div>
                        <div class="input">
                            <input class="button" type="submit" value="Sign Up">
                        </div>
                        <p>or</p>
                        <a class="login" href="#">Login</a>
                    </form>
                </div>
                <div class="sign-in">
                    <h3>Login</h3>
                    <p>Start your own campaign and fund your dreams. Simple as this.</p>
                    <form method="post" id="login" class="ajax-auth">
                        <div class="input">
                            <label>Username</label>
                            <input class="username" type="text" name="username_log" autocomplete="off">
                        </div>
                        <div class="input">
                            <label>Password</label>
                            <input type="password" name="password_log" autocomplete="off"><i class="icon-eye"></i>
                        </div>
                        <a class="forgot-password-btn" href="#">Forgot your password?</a>
                        <div class="input">
                            <input class="button" type="submit" value="Login">
                        </div>
                        <p>or</p>
                        <a class="sign-up-btn" href="#">Sign Up</a>
                    </form>
                </div>
                <div class="forgot-password">
                    <h3>Reset Password</h3>
                    <p>Enter your email to reset your password</p>
                    <form>
                        <div class="input">
                            <label>Email</label>
                            <input type="email">
                        </div>
                        <div class="input">
                            <input class="button" type="submit" value="Reset Pssword">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/build.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/flipclock.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/ajax-register-login.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/validate.js"></script>
    
    <script>
        $('.block-clock').each(function(){
            var el = $(this).children('[class^="clock-"], [class*=" clock-"]'),
                elDate = el.data('date');
            var clock = el.FlipClock(new Date(elDate).getTime() / 1000 - new Date().getTime() / 1000, {
                clockFace: 'DailyCounter',
                countdown: true,
                showSeconds: false
            });
        });
    </script>
    <?php if(is_page('account')) { ?>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/core.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/charts.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/animated.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/dark.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/chart.js"></script>
    <?php } ?>
    </body>
</html>