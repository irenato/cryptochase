<?php get_header(); 
$footer_img = get_field('footer_image', 2); ?>

<section>
    <div class="banner-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php
                    $args = array( 
                        'post_type'      => 'promos', 
                        'posts_per_page' => 1,
                        'category_name'  => 'generic-promo'
                    );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post(); 
                        $attachment_image   = get_the_post_thumbnail_url(get_the_ID()); ?>
                            <a href="<?php echo get_post_permalink(); ?>">
                                <h1><?php echo get_the_title(); ?></h1>
                                <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                            </a>
                        <?php 
                    endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="table-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span>Sign up at eligible CyptoChase partner platforms and trade or gamble cryptocurrency to earn your share of $3k every month.</span>
                    <h2>Leaderboard Standing</h2>
                    <a class="table" href="#">
                        <?php the_field('table_generic', 215); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="deals">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Crypto Deals</h2>
                    <div class="items">
                        <?php if( have_rows('reviews_home_q', 2) ): ?>
                            <div class="item">
                                <h3>Crypto CFD Deals</h3>
                                <ul>
                                    <?php while( have_rows('reviews_home_q', 2) ): the_row(); 
                                        $title_reviews          = get_sub_field('title_reviews');
                                        $description_reviews    = get_sub_field('description_reviews');
                                        $rating_reviews         = get_sub_field('rating_reviews');
                                        $logo_reviews           = get_sub_field('logo_reviews');
                                        $url_reviews            = get_sub_field('url_reviews');
                                        $url_reviews_sign_up    = get_sub_field('url_reviews_sign_up'); ?>
                                        <li>
                                            <a href="<?php echo $url_reviews; ?>" target="_blank">
                                                <div class="img"><img src="<?php echo $logo_reviews['url']; ?>" alt="#"></div>
                                                <div class="desc">
                                                    <h6><?php echo $title_reviews; ?><span class="icon-star"><?php echo $rating_reviews; ?> </span></h6>
                                                    <p><?php echo $description_reviews; ?></p>
                                                </div>
                                            </a>
                                            <div class="link"><a class="button" href="<?php echo $url_reviews_sign_up; ?>">Sign Up</a><a class="review" href="<?php echo $url_reviews; ?>">Review</a></div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        <?php if( have_rows('reviews_home_w', 2) ): ?>
                            <div class="item">
                                <h3>Crypto Exchange Deals</h3>
                                <ul>
                                    <?php while( have_rows('reviews_home_w', 2) ): the_row(); 
                                        $title_reviews          = get_sub_field('title_reviews_2');
                                        $description_reviews    = get_sub_field('description_reviews_2');
                                        $rating_reviews         = get_sub_field('rating_reviews_2');
                                        $logo_reviews           = get_sub_field('logo_reviews_2');
                                        $url_reviews            = get_sub_field('url_reviews_2'); 
                                        $url_reviews_sign_up    = get_sub_field('url_reviews_sign_up_2'); ?>
                                        
                                        <li>
                                            <a href="#" target="_blank">
                                                <div class="img"><img src="<?php echo $logo_reviews['url']; ?>" alt="#"></div>
                                                <div class="desc">
                                                    <h6><?php echo $title_reviews; ?><span class="icon-star"><?php echo $rating_reviews; ?> </span></h6>
                                                    <p><?php echo $description_reviews; ?></p>
                                                </div>
                                            </a>
                                            <div class="link"><a class="button" href="<?php echo $url_reviews_sign_up; ?>">Sign Up</a><a class="review" href="<?php echo $url_reviews; ?>">Review</a></div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        <?php if( have_rows('reviews_home_e', 2) ): ?>  
                            <div class="item">
                                <h3>Crypto Casino Deals</h3>
                                <ul>
                                    <?php while( have_rows('reviews_home_e', 2) ): the_row(); 
                                        $title_reviews          = get_sub_field('title_reviews_3');
                                        $description_reviews    = get_sub_field('description_reviews_3');
                                        $rating_reviews         = get_sub_field('rating_reviews_3');
                                        $logo_reviews           = get_sub_field('logo_reviews_3');
                                        $url_reviews            = get_sub_field('url_reviews_3'); 
                                        $url_reviews_sign_up    = get_sub_field('url_reviews_sign_up_3'); ?>
                                        <li>
                                            <a href="#" target="_blank">
                                                <div class="img"><img src="<?php echo $logo_reviews['url']; ?>" alt="#"></div>
                                                <div class="desc">
                                                    <h6><?php echo $title_reviews; ?><span class="icon-star"><?php echo $rating_reviews; ?> </span></h6>
                                                    <p><?php echo $description_reviews; ?></p>
                                                </div>
                                            </a>
                                            <div class="link"><a class="button" href="<?php echo $url_reviews_sign_up; ?>">Sign Up</a><a class="review" href="<?php echo $url_reviews; ?>">Review</a></div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="friends">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Friends of CryptoChase</h2>
                    <div class="items">
                        <?php if( have_rows('friends_of_cryptochase', 2) ): ?>  
                            <?php while( have_rows('friends_of_cryptochase', 2) ): the_row(); 
                                $image_friends_of_cryptochase           = get_sub_field('image_friends_of_cryptochase');
                                $url_friends_of_cryptochase             = get_sub_field('url_friends_of_cryptochase'); 
                                $title_friends_of_cryptochase           = get_sub_field('title_friends_of_cryptochase'); 
                                $description_friends_of_cryptochase     = get_sub_field('description_friends_of_cryptochase'); ?>
                                <a class="item" href="<?php echo $url_friends_of_cryptochase; ?>">
                                    <div class="img"><img src="<?php echo $image_friends_of_cryptochase['url']; ?>" alt="#"></div>
                                    <h4><?php echo $title_friends_of_cryptochase; ?><span><?php echo $description_friends_of_cryptochase; ?></span></h4>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="partners">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>As Seen On</h2>
                    <div class="items">
                        <?php if( have_rows('as_seen_on', 2) ): ?>  
                            <?php while( have_rows('as_seen_on', 2) ): the_row(); 
                                $image_as_seen_on  = get_sub_field('image_as_seen_on');
                                $url_as_seen_on    = get_sub_field('url_as_seen_on'); ?>
                                <a class="item" href="<?php echo $url_as_seen_on; ?>"><img src="<?php echo $image_as_seen_on['url']; ?>" alt="#"></a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="girl" src="<?php echo $footer_img['url']; ?>" alt="#">
</section>
<?php get_footer(); ?>