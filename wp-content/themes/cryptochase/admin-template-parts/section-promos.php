<?php
$rules = getPromoRules($post->ID);
?>

<table class="form-table">
    <tbody>
    <tr class="acf-field acf-field-repeater">
        <td class="acf-input">
            <div class="acf-repeater -table">
                <table class="acf-table">

                    <thead>
                    <tr>
                        <th class="acf-th" style="width: 33.33%;">
                            Position
                        </th>
                        <th class="acf-th" style="width: 33.33%;">
                            Prize
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if ($rules):
                        foreach ($rules as $rule): ?>
                            <tr class="acf-row">
                                <td class="acf-field acf-field-text">
                                    <?= $rule['position'] ?>
                                </td>
                                <td class="acf-field acf-field-text">
                                    <?= $rule['prize'] ?>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>

            </div>
        </td>
    </tr>
    </tbody>
</table>
