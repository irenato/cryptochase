<?php
$platforms = getCustomPosts('platforms');
$user = get_query_var('user');
$ptsData = get_query_var('ptsData');

?>

<table class="form-table">
    <tbody>
    <tr class="acf-field acf-field-repeater acf-field-5d47fb44798e8" data-name="user_platforms" data-type="repeater"
        data-key="field_5d47fb44798e8">
        <td class="acf-label">
            <label for="acf-field_5d47fb44798e8">Platforms</label></td>
        <td class="acf-input">
            <div class="acf-repeater -table" data-min="0" data-max="0">
                <input name="acf[field_5d47fb44798e8]" value="" type="hidden">
                <table class="acf-table">

                    <thead>
                    <tr>
                        <th class="acf-th" data-name="image_platform" data-type="text" data-key="field_5d4802db92cd5"
                            style="width: 50%;">
                            Platform
                        </th>
                        <th class="acf-th" data-name="name_user_platforms" data-type="text"
                            data-key="field_5d47fb57798e9" style="width: 50%;">
                            Name
                        </th>

                        <th class="acf-row-handle"></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if ($ptsData):
                        foreach ($ptsData as $pts): ?>
                            <tr class="acf-row">


                                <td class="acf-field acf-field-text" data-name="image_platform">
                                    <div class="acf-input">
                                        <div class="acf-input-wrap">
                                            <select name="pts[<?= $pts->id ?>][partner_id]">
                                                <option>Choose something</option>
                                                <?php foreach ($platforms as $platform): ?>
                                                    <option value="<?= $platform->ID ?>" <?= $platform->ID == $pts->partner_id ? 'selected="selected"' : '' ?>><?= $platform->post_title ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </td>

                                <td class="acf-field acf-field-text">
                                    <div class="acf-input">
                                        <div class="acf-input-wrap">
                                            <input type="text"
                                                   name="pts[<?= $pts->id ?>][username]"
                                                   value="<?= $pts->username ?>">
                                        </div>
                                    </div>
                                </td>
                                <td class="acf-row-handle remove">
                                    <a class="acf-icon -minus small acf-js-tooltip js-remove-user" data-id="<?= $pts->id ?>" href="#"
                                       title="Remove row"></a>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                    endif;
                    ?>
                    <tr class="acf-row">

                        <td class="acf-field acf-field-text">
                            <div class="acf-input">
                                <div class="acf-input">
                                    <select name="pts[new][partner_id]">
                                        <option value="0" selected>Choose something</option>
                                        <?php foreach ($platforms as $platform): ?>
                                            <option value="<?= $platform->ID ?>"><?= $platform->post_title ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </td>

                        <td class="acf-field acf-field-text">
                            <div class="acf-input">
                                <div class="acf-input-wrap">
                                    <input type="text"
                                           name="pts[new][username]"
                                           value="">
                                </div>
                            </div>
                        </td>

                        <td class="acf-row-handle remove">
                            <a class="acf-icon -minus small acf-js-tooltip" href="#" data-event="remove-row"
                               title="Remove row"></a>
                        </td>

                    </tr>
                    </tbody>
                </table>

            </div>
        </td>
    </tr>
    </tbody>
</table>
