<?php
add_theme_support('title-tag');

require_once(get_stylesheet_directory() . '/inc/shortcodes/guide-block.php');
require_once(get_stylesheet_directory() . '/inc/shortcodes/team-block.php');
require_once(get_stylesheet_directory() . '/inc/shortcodes/partners-block.php');
/*require_once(get_stylesheet_directory() . '/inc/shortcodes/wallet-block.php');
require_once(get_stylesheet_directory() . '/inc/shortcodes/exchange-block.php');*/
require_once(get_stylesheet_directory() . '/inc/shortcodes/white-paper-block.php');
require_once(get_stylesheet_directory() . '/inc/shortcodes/graph-block.php');
require_once(get_stylesheet_directory() . '/inc/shortcodes/graph-block.php');
require_once('inc/custom/platforms.php');

function pw_loading_scripts_wrong_again()
{
    wp_enqueue_script('admin_js', get_stylesheet_directory_uri() . '/js/admin.js');
}

add_action('admin_init', 'pw_loading_scripts_wrong_again');

add_action('wp_ajax_ajax_register', 'ajax_register');
add_action('wp_ajax_nopriv_ajax_register', 'ajax_register');
function ajax_register()
{
    $name = $_POST['get_name'];
    $email = $_POST['get_email'];
    $username = $_POST['get_username'];
    $password = $_POST['get_password'];

    $userdata = array(
        'user_pass' => $password,
        'user_login' => $username,
        'user_nicename' => $name,
        'user_url' => '',
        'user_email' => $email,
        'display_name' => '',
        'nickname' => '',
        'first_name' => '',
        'last_name' => '',
        'description' => '',
        'rich_editing' => 'true',
        'user_registered' => '',
        'role' => get_option('default_role'),
        'jabber' => '',
        'aim' => '',
        'yim' => '',
    );

    $user_id = wp_insert_user($userdata);

    echo '<p class="congratulations">Congratulations! You was successfully registered at the Crypto Chase. Please, check your email for the details.</p>';

    $headers[] = 'Content-type: text/html; charset=utf-8';
    $message = '
    Hi ' . $username . ',<br>
    Thank you for registering! <br><br>

    You login:<br>
    ' . $username . '<br><br>

    You password:<br>
    ' . $password . '<br><br>';

    wp_mail($email, 'Thank you for registering', $message, $headers);

    wp_die();
}

add_action('wp_ajax_ajax_loginr', 'ajax_login');
add_action('wp_ajax_nopriv_ajax_login', 'ajax_login');
function ajax_login()
{
    $info = array();

    $username = $_POST['get_username'];
    $password = $_POST['get_password'];

    $info['user_login'] = $username;
    $info['user_password'] = $password;
    $info['remember'] = true;

    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false, 'message' => __('Неправильный логин или пароль!')));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => __('Отлично! Идет перенаправление...')));
    }

    die();
}

add_action('init', 'my_custom_init');
function my_custom_init()
{
    register_post_type('coin', array(
        'labels' => array(
            'name' => 'Coins',
            'singular_name' => 'Coin',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Coin',
            'edit_item' => 'Edit Coin',
            'new_item' => 'New Coin',
            'view_item' => 'View Coin',
            'search_items' => 'Search Coins',
            'not_found' => 'No Coins found',
            'not_found_in_trash' => 'No Coins found in Trash.',
            'parent_item_colon' => '',
            'menu_name' => 'Coins'
        ),
        'menu_icon' => 'https://cryptochase.io/wp-content/uploads/2019/07/wpdashboard_coins.png',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 2,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
    ));

    register_post_type('promos', array(
        'labels' => array(
            'name' => 'Promos',
            'singular_name' => 'Promos',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Promos',
            'edit_item' => 'Edit Promos',
            'new_item' => 'New Promos',
            'view_item' => 'View Promos',
            'search_items' => 'Search Promos',
            'not_found' => 'No Promos found',
            'not_found_in_trash' => 'No Promos found in Trash.',
            'parent_item_colon' => '',
            'menu_name' => 'Promos'
        ),
        'menu_icon' => 'https://cryptochase.io/wp-content/uploads/2019/07/wpdashboard_promos.png',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 2,
        'supports' => array('title', 'author', 'thumbnail'),
        'taxonomies' => array('category'),
    ));

    register_post_type('platforms', array(
        'labels' => array(
            'name' => 'Platforms',
            'singular_name' => 'Platforms',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Platforms',
            'edit_item' => 'Edit Platforms',
            'new_item' => 'New Platforms',
            'view_item' => 'View Platforms',
            'search_items' => 'Search Platforms',
            'not_found' => 'No Platforms found',
            'not_found_in_trash' => 'No Platforms found in Trash.',
            'parent_item_colon' => '',
            'menu_name' => 'Platforms'
        ),
        'menu_icon' => 'https://cryptochase.io/wp-content/uploads/2019/07/wpdashboard_platforms.png',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 2,
        'supports' => array('title', 'thumbnail')
    ));
}

function restrict_admin()
{
    if (!current_user_can('manage_options') && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF']) {
        wp_redirect(site_url());
    }
}

add_action('admin_init', 'restrict_admin', 1);

function getPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}

function setPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

register_sidebar(array(
    'name' => 'Advertising',
    'id' => 'information_widget',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

class informationWidget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'information_widget',
            'Information',
            array('description' => '')
        );
    }

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);
        $image_uri = apply_filters('widget_image', $instance['image_uri']);
        $image_url = apply_filters('widget_url', $instance['image_url']);

        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo '<a href="' . $instance['image_url'] . '"><img src="' . $instance['image_uri'] . '" alt="#"></a>';
    }

    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        }
        if (isset($instance['image_uri'])) {
            $image_uri = $instance['image_uri'];
        }
        if (isset($instance['image_url'])) {
            $image_url = $instance['image_url'];
        } ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('image_url'); ?>">URL</label>
            <input class="widefat" id="<?php echo $this->get_field_id('image_url'); ?>"
                   name="<?php echo $this->get_field_name('image_url'); ?>" type="text"
                   value="<?php echo esc_attr($image_url); ?>"/>
        </p>
        <p>
            <label for="<?= $this->get_field_id('image_uri'); ?>">Image</label>
            <img class="<?= $this->id ?>_img"
                 src="<?= (!empty($instance['image_uri'])) ? $instance['image_uri'] : ''; ?>"
                 style="margin:0;padding:0;max-width:100%;display:block"/>
            <input type="text" class="widefat <?= $this->id ?>_url" name="<?= $this->get_field_name('image_uri'); ?>"
                   value="<?= $instance['image_uri']; ?>" style="margin-top:5px;"/>
            <input type="button" id="<?= $this->id ?>" class="button button-primary js_custom_upload_media"
                   value="Upload Image" style="margin-top:5px;"/>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['image_url'] = (!empty($new_instance['image_url'])) ? strip_tags($new_instance['image_url']) : '';
        $instance['image_uri'] = (!empty($new_instance['image_uri'])) ? strip_tags($new_instance['image_uri']) : '';
        return $instance;
    }
}

function information_widget_load()
{
    register_widget('informationWidget');
}

add_action('widgets_init', 'information_widget_load');

if (function_exists('acf_add_options_page')) {
    $args = array(
        'page_title' => 'Information crypto',
        'post_id' => 'information_crypto',
        'icon_url' => false,
    );
    acf_add_options_page($args);

    $dropdowns = array(
        'page_title' => 'Dropdowns',
        'icon_url' => 'https://cryptochase.io/wp-content/uploads/2019/07/wpdashboard_dropdown.png',
        'position' => 7
    );
    acf_add_options_page($dropdowns);
}

add_filter('cron_schedules', 'cron_add_one_min');
function cron_add_one_min($schedules)
{
    $schedules['one_min'] = array(
        'interval' => 60 * 15,
        'display' => '15 minutes'
    );
    return $schedules;
}

add_action('wp', 'criptocoin_api');
function criptocoin_api()
{
    if (!wp_next_scheduled('criptocoin_api_request')) {
        wp_schedule_event(time(), 'one_min', 'criptocoin_api_request');
    }
}

$flag = true;

add_action('criptocoin_api_request', 'criptocoin_api_request_function', 10, 0);
function criptocoin_api_request_function()
{
    $url = 'https://pro-api.coinmarketcap.com/v1/global-metrics/quotes/latest';
    $parameters = [
        'convert' => 'USD'
    ];

    $headers = [
        'Accepts: application/json',
        'X-CMC_PRO_API_KEY: 1acbdeb3-2a28-43cc-b93a-ff305a683379'
    ];
    $qs = http_build_query($parameters);
    $request = "{$url}?{$qs}";

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $request,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => 1
    ));

    $response = curl_exec($curl);
    $get_data = json_decode($response);
    curl_close($curl);

    $market_сap = $get_data->data->quote->USD->total_market_cap;
    $h_volume = $get_data->data->quote->USD->total_volume_24h;
    $btc_dominance = $get_data->data->btc_dominance;

    update_field('market_сap', $market_сap, 'information_crypto');
    update_field('24h_volume', $h_volume, 'information_crypto');
    update_field('btc_dominance', $btc_dominance, 'information_crypto');
}

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

function f_parse_csv($file, $longest, $delimiter)
{
    $mdarray = array();
    $file = fopen($file, "r");
    while ($line = fgetcsv($file, $longest, $delimiter)) {
        array_push($mdarray, $line);
    }
    fclose($file);
    return $mdarray;
}

add_action('wp_ajax_ajax_add_platforms', 'ajax_add_platforms');
add_action('wp_ajax_nopriv_ajax_add_platforms', 'ajax_add_platforms');
function ajax_add_platforms()
{
    global $wpdb;
    $wpdb->insert($wpdb->prefix . 'users_partners', [
        'user_id' => (int)get_current_user_id(),
        'partner_id' => (int)$_POST['platformId'],
        'username' => esc_sql($_POST['userName']),
    ]);
}

add_action('admin_head',
    'my_custom_fonts'); // admin_head is a hook my_custom_fonts is a function we are adding it to the hook
function my_custom_fonts()
{
    echo '<style>
  	#menu-posts {
		display: none;
    }
    
    #menu-comments {
		display: none;
    }

    #toplevel_page_acf-options-information-crypto {
		display: none;
    }
    
    #adminmenu li.wp-menu-separator {
        display: none;
    }

    #acf-group_5d2eda8d59509 {
        display: none !important;
    }
  </style>';
}