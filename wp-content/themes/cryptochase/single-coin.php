<?php 
/* 
* Template Name: Coin
*/ 

get_header();

$url_coin           = get_field("url_coin");
$telegram_social    = get_field("telegram_social");
$facebook_social    = get_field("facebook_social");
$twitter_social     = get_field("twitter_social");
$youtube_social     = get_field("youtube_social");
$instagram_social   = get_field("instagram_social");
$medium_social      = get_field("medium_social");

$exchange_coin    = get_field("exchange_coin");
$market_cap_coin  = get_field("market_cap_coin");
$h_vol_coin       = get_field("h_vol_coin");
$negative_coin    = get_field("negative_coin");
$coin_supply_coin = get_field("coin_supply_coin");

setPostViews(get_the_ID()); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="bread-crumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="/coins/">Coins</a></li>
                    <li><a href="#"><?php the_title(); ?></a></li>
                </ul>
                <div class="coin">
                    <div class="content">
                        <p class="info"><strong>$<?php echo $exchange_coin; ?></strong><i class="icon-arrow-bottom"><?php echo $negative_coin; ?></i></p>
                        <div class="soc">
                            <ul>
                                <?php 
                                if(!empty($telegram_social)) {
                                    echo '<li><a class="icon-telegram" href="' . $telegram_social . '"></a></li>';
                                }
                                if(!empty($facebook_social)) {
                                    echo '<li><a class="icon-facebook" href="' . $facebook_social . '"></a></li>';
                                }
                                if(!empty($twitter_social)) {
                                    echo '<li><a class="icon-twitter" href="' . $twitter_social . '"></a></li>';
                                }
                                if(!empty($youtube_social)) {
                                    echo '<li><a class="icon-youtube" href="' . $youtube_social . '"></a></li>';
                                }
                                if(!empty($instagram_social)) {
                                    echo '<li><a class="icon-instagram" href="' . $instagram_social . '"></a></li>';
                                }
                                if(!empty($medium_social)) {
                                    echo '<li><a class="icon-medium" href="' . $medium_social . '"></a></li>';
                                } ?>
                            </ul>
                            <a href="<?php echo $url_coin; ?>"><?php echo $url_coin; ?></a>
                        </div>
                        <div class="clearfix"></div>

                        <ul class="description">
                            <li><strong>Market Сap:</strong>$<?php echo $market_cap_coin; ?></li>
                            <li><strong>Coin Supply:</strong><?php echo $coin_supply_coin; ?></li>
                            <li><strong>24H Vol:</strong>$<?php echo $h_vol_coin; ?></li>
                        </ul>  
                        <div class="top-items">
                            <div class="item">
                                <h3>Top Wallets</h3>
                                <ul>
                                    <?php if( have_rows('top_wallets') ): ?>
                                        <?php while( have_rows('top_wallets') ): the_row(); 
                                            $logo_wallets        = get_sub_field('logo_wallets');
                                            $name_wallets        = get_sub_field('name_wallets');
                                            $rating_wallets      = get_sub_field('rating_wallets');
                                            $buy_now_url_wallets = get_sub_field('buy_now_url_wallets');
                                            $review_url_wallets  = get_sub_field('review_url_wallets');?>
                                            <li>
                                                <div class="img"><img src="<?php echo $logo_wallets['url']; ?>" alt="#"></div>
                                                <div class="desc">
                                                <h5><?php echo $name_wallets; ?><span class="icon-star"><?php echo $rating_wallets; ?> </span></h5>
                                                </div>
                                                <div class="link">
                                                    <a class="button" href="<?php echo $buy_now_url_wallets; ?>">Buy Now</a>
                                                    <a class="review" href="<?php echo $review_url_wallets; ?>">Review</a>
                                                </div>
                                            </li>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="item">
                                <h3>Top Exchanges</h3>
                                <ul>
                                    <?php if( have_rows('top_exchanges') ): ?>
                                        <?php while( have_rows('top_exchanges') ): the_row(); 
                                            $logo_exchanges        = get_sub_field('logo_exchanges');
                                            $name_exchanges        = get_sub_field('name_exchanges');
                                            $rating_exchanges      = get_sub_field('rating_exchanges');
                                            $buy_now_url_exchanges = get_sub_field('buy_now_url_exchanges');
                                            $review_url_exchanges  = get_sub_field('review_url_exchanges');?>
                                            <li>
                                                <div class="img"><img src="<?php echo $logo_exchanges['url']; ?>" alt="#"></div>
                                                <div class="desc">
                                                <h5><?php echo $name_exchanges; ?><span class="icon-star"><?php echo $rating_exchanges; ?> </span></h5>
                                                </div>
                                                <div class="link">
                                                    <a class="button" href="<?php echo $buy_now_url__exchanges; ?>">Buy Now</a>
                                                    <a class="review" href="<?php echo $review_url_exchanges; ?>">Review</a>
                                                </div>
                                            </li>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>      
                        <?php 
                        if ( have_posts() ) :  while ( have_posts() ) : the_post();
                            the_content();
                        endwhile; endif; ?>
                    </div>
                    <div class="sitebar">
                        <h3>Chase Crypto On Twitter</h3>
                        <?php echo do_shortcode('[custom-twitter-feeds]'); ?>
                        <?php if (is_active_sidebar('information_widget')) : ?>
                            <?php dynamic_sidebar('information_widget'); ?>
                        <?php endif; ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>