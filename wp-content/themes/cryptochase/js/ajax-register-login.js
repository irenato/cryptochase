jQuery(document).ready(function ($) {
    $('form#register').on('submit', function (e) {
        e.preventDefault();
        var get_name = $("input[name=name]").val();
        var get_email = $("input[name=email]").val();
        var get_username = $("input[name=username]").val();
        var get_password = $("input[name=password]").val();

        $.ajax({
            type: "POST",
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'ajax_register',
                get_name: get_name,
                get_email: get_email,
                get_username: get_username,
                get_password: get_password,
            },
            success: function (data) {
                $('.checkbox').after(data);
            },
        });
    });

    $('form#login').on('submit', function (e) {
        e.preventDefault();
        var get_username = $("input[name=username_log]").val();
        var get_password = $("input[name=password_log]").val();

        console.log(get_username);
        console.log(get_password);

        $.ajax({
            type: "POST",
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'ajax_login',
                get_username: get_username,
                get_password: get_password,
            },
            success: function (data) {
                document.location.href = '/account/';
            },
        });
    });

    $('.send_platform').on('click', function (e) {
        e.preventDefault();
        var platformId = $(this).parents('.item').find('.image_platform').data('id'),
            platformImage = $(this).parents('.item').find('.image_platform').attr('src'),
            userName = $(this).parents('.item').find('input').val();

        if (userName.length < 6) {
            $('#alert_form_error').modal('show');
        } else if (userName.length > 15) {
            $('#alert_form_error').modal('show');
        } else if (/[А-яЁё]/.test(userName)) {
            $('#alert_form_error').modal('show');
        } else {
            $.ajax({
                type: "POST",
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'ajax_add_platforms',
                    platformId: platformId,
                    userName: userName
                },
                success: function (data) {
                    $('.bound ul').after('<li><div class="img"><img src="' + platformImage + '" alt=""></div><p>' + userName + '</p></li>');
                    $('#alert_form_ok').modal('show');
                    $('.username_account').val('');
                    $('.username_account').removeClass('success');
                },
            });
        }
    });
    
    $(document).on('click', '.js-month', function (e) {
        var __this = $(this);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'ajaxShowPromoData',
                date: $(__this).data('date')
            },
            success: function (data) {
                $('.js-report-prise').html(data.prizes ? '$' + data.prizes : 0);
                $('.js-report-amount').html(data.amount);
            },
        });
        $.ajax({
            type: "POST",
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'ajaxShowContent',
                date: $(__this).data('date')
            },
            success: function (data) {
                $('.js-content-inner').html(data);
            },
        });
    })
});

