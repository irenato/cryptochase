$(document).ready(function(){
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height() - 200) {
            $.ajax({
                type: "POST",
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'add_promo'
                },
                success: function( data ) {
                    $('.tbody').append(data);
                },
            });
        }
    });
});
