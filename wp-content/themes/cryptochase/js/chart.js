am4core.ready(function() {
	am4core.useTheme(am4themes_dark);
	am4core.useTheme(am4themes_animated);
	// Themes end

	var chart = am4core.create("chartdiv", am4charts.XYChart);

	var data = [];

    chart.data = $('#chartdiv').data('values');

	var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.ticks.template.disabled = true;
		categoryAxis.renderer.line.opacity = 0;
		categoryAxis.renderer.grid.template.disabled = true;
		categoryAxis.renderer.minGridDistance = 40;
		categoryAxis.dataFields.category = "year";
		categoryAxis.startLocation = 0.4;
		categoryAxis.endLocation = 0.6;
		categoryAxis.stroke = am4core.color("#fff");
		categoryAxis.fill = am4core.color("#fff");
		categoryAxis.color = am4core.color("#fff");


	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.tooltip.disabled = true;
		valueAxis.renderer.line.opacity = 1;
		valueAxis.renderer.ticks.template.disabled = true;
		valueAxis.min = 0;
		valueAxis.stroke = am4core.color("#fff");
		valueAxis.fill  = am4core.color("#fff");

	var lineSeries = chart.series.push(new am4charts.LineSeries());
		lineSeries.dataFields.categoryX = "year";
		lineSeries.dataFields.valueY = "points";
		lineSeries.tooltipText = "points: {valueY.value}";
		lineSeries.fillOpacity = 1;
		lineSeries.fill = am4core.color("#A32D66")
		lineSeries.strokeWidth = 3;
		lineSeries.stroke = am4core.color("#E43F8F")
		console.log(lineSeries)

	var bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
		bullet.circle.radius = 6;
		bullet.circle.fill = am4core.color("#fff");
		bullet.circle.strokeWidth = 3;

	chart.cursor = new am4charts.XYCursor();
	chart.cursor.behavior = "panX";
	chart.cursor.lineX.opacity = 1;
	chart.cursor.lineY.opacity = 1;
	
	
	chart.cursor = new am4charts.XYCursor();
	chart.cursor.behavior = "panX";
	chart.cursor.lineX.opacity = 0;
	chart.cursor.lineY.opacity = 0;

	chart.scrollbarX = new am4core.Scrollbar();
	chart.scrollbarX.parent = chart.bottomAxesContainer;
});