<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/flipclock.css" rel="stylesheet">
        <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet">
        <?php wp_head(); ?>
    </head>
    <body>
        <header>
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="left-block">
                                <a class="logo" href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg" alt="logo"></a>
                                <div class="burger-menu"> <span></span><span></span><span></span></div>
                            </div>
                            <div class="right-block">
                                <ul class="soc">
                                    <li><a class="icon-facebook" href="https://www.facebook.com/" target="_blank"> </a></li>
                                    <li><a class="icon-twitter" href="https://twitter.com/" target="_blank"></a></li>
                                    <li><a class="icon-youtube" href="https://www.youtube.com/" target="_blank"></a></li>
                                    <li><a class="icon-telegram" href="https://web.telegram.org/" target="_blank"></a></li>
                                    <li><a class="icon-pinterest" href="https://www.pinterest.com/#" target="_blank"></a></li>
                                </ul>
                                <a class="button" href="#">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul class="menu">
                                <?php if (is_user_logged_in()) { ?>
                                    <li><a href="/account/">Account</a></li>
                                <?php } else { get_template_part('ajax', 'auth'); ?>             
                                    <li><a href="#" data-toggle="modal" data-target=".sign-up-popup">Account</a></li>
                                <?php } ?>
                                <li><a href="/listings-promos/">Promos</a></li>
                                <li class="menu-item-has-children">
                                    <a href="#">Reviews</a><i class="icon-arrow-bottom"></i>
                                    <ul class="submenu">
                                        <li>
                                            <?php if( have_rows('reviews_menu', 'option') ): ?>
                                                <div class="items ">
                                                    <h5>Crypto CFD Deals</h5>
                                                    <?php while( have_rows('reviews_menu', 'option') ): the_row(); 
                                                        $title_reviews          = get_sub_field('title_reviews');
                                                        $description_reviews    = get_sub_field('description_reviews');
                                                        $rating_reviews         = get_sub_field('rating_reviews');
                                                        $logo_reviews           = get_sub_field('logo_reviews');
                                                        $icon_cup_reviews       = get_sub_field('icon_cup_reviews');
                                                        $select_item_reviews    = get_sub_field('select_item_reviews');
                                                        $url_reviews            = get_sub_field('url_reviews'); ?>

                                                        <a class="item icon-cup <?php echo $icon_cup_reviews; ?>" href="<?php echo $url_reviews; ?>" target="_blank">
                                                            <div class="img"><img src="<?php echo $logo_reviews['url']; ?>" alt="#"></div>
                                                            <div class="desc">
                                                                <h6><?php echo $title_reviews; ?><span class="icon-star"><?php echo $rating_reviews; ?> </span></h6>
                                                                <p><?php echo $description_reviews; ?></p>
                                                            </div>
                                                        </a>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if( have_rows('reviews_menu_2', 'option') ): ?>
                                                <div class="items ">
                                                    <h5>Crypto Exchange Deals</h5>
                                                    <?php while( have_rows('reviews_menu_2', 'option') ): the_row(); 
                                                        $title_reviews          = get_sub_field('title_reviews_2');
                                                        $description_reviews    = get_sub_field('description_reviews_2');
                                                        $rating_reviews         = get_sub_field('rating_reviews_2');
                                                        $logo_reviews           = get_sub_field('logo_reviews_2');
                                                        $icon_cup_reviews       = get_sub_field('icon_cup_reviews_2');
                                                        $select_item_reviews    = get_sub_field('select_item_reviews_2');
                                                        $url_reviews            = get_sub_field('url_reviews_2'); ?>
                                                        
                                                        <a class="item icon-cup <?php echo $icon_cup_reviews; ?>" href="<?php echo $url_reviews; ?>" target="_blank">
                                                            <div class="img"><img src="<?php echo $logo_reviews['url']; ?>" alt="#"></div>
                                                            <div class="desc">
                                                                <h6><?php echo $title_reviews; ?><span class="icon-star"><?php echo $rating_reviews; ?> </span></h6>
                                                                <p><?php echo $description_reviews; ?></p>
                                                            </div>
                                                        </a>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if( have_rows('reviews_menu_3', 'option') ): ?>
                                                <div class="items ">
                                                    <h5>Crypto Casino Deals</h5>
                                                    <?php while( have_rows('reviews_menu_3', 'option') ): the_row(); 
                                                        $title_reviews          = get_sub_field('title_reviews_3');
                                                        $description_reviews    = get_sub_field('description_reviews_3');
                                                        $rating_reviews         = get_sub_field('rating_reviews_3');
                                                        $logo_reviews           = get_sub_field('logo_reviews_3');
                                                        $icon_cup_reviews       = get_sub_field('icon_cup_reviews_3');
                                                        $select_item_reviews    = get_sub_field('select_item_reviews_3');
                                                        $url_reviews            = get_sub_field('url_reviews_3'); ?>
                                                        
                                                        <a class="item icon-cup <?php echo $icon_cup_reviews; ?>" href="<?php echo $url_reviews; ?>" target="_blank">
                                                            <div class="img"><img src="<?php echo $logo_reviews['url']; ?>" alt="#"></div>
                                                            <div class="desc">
                                                                <h6><?php echo $title_reviews; ?><span class="icon-star"><?php echo $rating_reviews; ?> </span></h6>
                                                                <p><?php echo $description_reviews; ?></p>
                                                            </div>
                                                        </a>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/coins/">Coins</a><i class="icon-arrow-bottom"></i>
                                    <ul class="submenu">
                                        <li>
                                            <h5>Our Top Coins To Watch</h5>
                                            <?php if( have_rows('coins_menu', 'option') ): ?>
                                                <div class="items ">
                                                    <h5>Big Cap</h5>
                                                    <?php while( have_rows('coins_menu', 'option') ): the_row(); 
                                                        $title_coins          = get_sub_field('title_coins');
                                                        $description_coins    = get_sub_field('description_coins');
                                                        $rating_coins         = get_sub_field('rating_coins');
                                                        $logo_coins           = get_sub_field('logo_coins');
                                                        $icon_cup_coins       = get_sub_field('icon_cup_coins');
                                                        $select_item_coins    = get_sub_field('select_item_coins');
                                                        $url_coins            = get_sub_field('url_coins'); ?>

                                                        <a class="item icon-cup <?php echo $icon_cup_coins; ?>" href="<?php echo $url_coins; ?>" target="_blank">
                                                            <div class="img"><img src="<?php echo $logo_coins['url']; ?>" alt="#"></div>
                                                            <div class="desc">
                                                                <h6><?php echo $title_coins; ?><span class="icon-star"><?php echo $rating_coins; ?> </span></h6>
                                                                <p><?php echo $description_coins; ?></p>
                                                            </div>
                                                        </a>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if( have_rows('coins_menu_2', 'option') ): ?>
                                                <div class="items ">
                                                    <h5>Mid Cap</h5>
                                                    <?php while( have_rows('coins_menu_2', 'option') ): the_row(); 
                                                        $title_coins          = get_sub_field('title_coins_2');
                                                        $description_coins    = get_sub_field('description_coins_2');
                                                        $rating_coins         = get_sub_field('rating_coins_2');
                                                        $logo_coins           = get_sub_field('logo_coins_2');
                                                        $icon_cup_coins       = get_sub_field('icon_cup_coins_2');
                                                        $select_item_coins    = get_sub_field('select_item_coins_2');
                                                        $url_coins            = get_sub_field('url_coins_2'); ?>
                                                        
                                                        <a class="item icon-cup <?php echo $icon_cup_coins; ?>" href="<?php echo $url_coins; ?>" target="_blank">
                                                            <div class="img"><img src="<?php echo $logo_coins['url']; ?>" alt="#"></div>
                                                            <div class="desc">
                                                                <h6><?php echo $title_coins; ?><span class="icon-star"><?php echo $rating_coins; ?> </span></h6>
                                                                <p><?php echo $description_coins; ?></p>
                                                            </div>
                                                        </a>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if( have_rows('coins_menu_3', 'option') ): ?>
                                                <div class="items ">
                                                    <h5>Small Cap</h5>
                                                    <?php while( have_rows('coins_menu_3', 'option') ): the_row(); 
                                                        $title_coins          = get_sub_field('title_coins_3');
                                                        $description_coins    = get_sub_field('description_coins_3');
                                                        $rating_coins         = get_sub_field('rating_coins_3');
                                                        $logo_coins           = get_sub_field('logo_coins_3');
                                                        $icon_cup_coins       = get_sub_field('icon_cup_coins_3');
                                                        $select_item_coins    = get_sub_field('select_item_coins_3');
                                                        $url_coins            = get_sub_field('url_coins_3'); ?>
                                                        
                                                        <a class="item icon-cup <?php echo $icon_cup_coins; ?>" href="<?php echo $url_coins; ?>" target="_blank">
                                                            <div class="img"><img src="<?php echo $logo_coins['url']; ?>" alt="#"></div>
                                                            <div class="desc">
                                                                <h6><?php echo $title_coins; ?><span class="icon-star"><?php echo $rating_coins; ?> </span></h6>
                                                                <p><?php echo $description_coins; ?></p>
                                                            </div>
                                                        </a>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php endif; ?>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>