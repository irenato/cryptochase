<?php
/**
 * ShortCode Graph
**/
class vcGraph extends WPBakeryShortCode {
    public function __construct() {
        add_action('init', array( $this, 'vc_graph_mapping' ));
        add_shortcode('vc_graph', array( $this, 'vc_graph_html' ));
    }

    public function vc_graph_mapping() {
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        vc_map(
            array(
                'name'          => __('Graph', 'cryptochase'),
                'base'          => 'vc_graph',
                'category'      => __('CryptoChase', 'cryptochase'),
                'icon'          =>  '',
                'params'        => array(
                    array(
                        'type'        => 'textarea_html',
                        'value'       => '',
                        'heading'     => __( 'Description:', 'villavona' ),
                        'param_name'  => 'content',
                    ),
                ),
            )
        );
    }

    public function vc_graph_html($atts, $content) {
        $avatar             = get_simple_local_avatar(get_the_author_meta('ID'), 120);
        $user_description   = get_the_author_meta('description');
        $user_nickname      = get_the_author_meta('nickname');
        $date_post          = get_the_date('F j, Y');
        
        $return = '
            <div class="tab-pane" id="graph">
                <div class="content_block">
                    ' . $content . '
                </div>
            </div>
        ';
        return $return;
    }
} 

new vcGraph();