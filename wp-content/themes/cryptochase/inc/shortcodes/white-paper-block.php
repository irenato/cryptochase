<?php
/**
 * ShortCode White paper
**/
class vcWhitePaper extends WPBakeryShortCode {
    public function __construct() {
        add_action('init', array( $this, 'vc_paper_mapping' ));
        add_shortcode('vc_paper', array( $this, 'vc_paper_html' ));
    }

    public function vc_paper_mapping() {
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        vc_map(
            array(
                'name'          => __('White paper', 'cryptochase'),
                'base'          => 'vc_paper',
                'category'      => __('CryptoChase', 'cryptochase'),
                'icon'          =>  '',
                'params'        => array(
                    array(
                        'type'        => 'textarea_html',
                        'value'       => '',
                        'heading'     => __( 'Description:', 'villavona' ),
                        'param_name'  => 'content',
                    ),
                ),
            )
        );
    }

    public function vc_paper_html($atts, $content) {
        $avatar             = get_simple_local_avatar(get_the_author_meta('ID'), 120);
        $user_description   = get_the_author_meta('description');
        $user_nickname      = get_the_author_meta('nickname');
        $date_post          = get_the_date('F j, Y');
        
        $return = '
            <div class="tab-pane" id="white-paper">
                <div class="content_block">
                    ' . $content . '
                </div>
            </div>
        ';
        return $return;
    }
} 

new vcWhitePaper();