<?php
/**
 * ShortCode Top Wallets
**/
class vcWallets extends WPBakeryShortCode {
    public function __construct() {
        add_action('init', array( $this, 'vc_wallets_mapping' ));
        add_shortcode('vc_wallets', array( $this, 'vc_wallets_html' ));
    }

    public function vc_wallets_mapping() {
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        vc_map(
            array(
                'name'          => __('Top Wallets', 'cryptochase'),
                'base'          => 'vc_wallets',
                'category'      => __('CryptoChase', 'cryptochase'),
                'icon'          =>  '',
                'params'        => array(
                    array(
                        'type'       => 'param_group',
                        'value'      => '',
                        'heading'     => __( 'Top Wallets:', 'villavona' ),
                        'param_name' => 'wallets_block',
                        'params'     => array(  
                            array(
                                'type'       => 'attach_image',
                                'value'      => '',
                                'heading'    => 'Logo:',
                                'param_name' => 'logo_repeater',
                            ),                       
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Name:',
                                'param_name' => 'name_repeater',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Rating:',
                                'param_name' => 'rating_repeater',
                            ),            
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Buy Now URL:',
                                'param_name' => 'buy_now_social',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Review URL:',
                                'param_name' => 'review_social',
                            ),
                        )
                    )
                ),
            )
        );
    }

    public function vc_wallets_html($atts, $content) {
        $wallets_block = vc_param_group_parse_atts($atts["wallets_block"]);

        foreach ($wallets_block as $wallets) {
            $photo_repeater     = wp_get_attachment_image_src($wallets['logo_repeater']);
            $name_repeater      = $wallets['name_repeater'];
            $rating_repeater    = $wallets['rating_repeater'];
            $buy_now_social     = $wallets['buy_now_social'];
            $review_social      = $wallets['review_social'];


            $short_code_block .= '
                <li>
                    <div class="img"><img src="' . $photo_repeater[0] . '" alt="#"></div>
                    <div class="desc">
                        <h5>' . $name_repeater . '<span class="icon-star">' . $rating_repeater . ' </span></h5>
                    </div>
                    <div class="link">
                        <a class="button" href="' . $buy_now_social . '">Buy Now</a>
                        <a class="review" href="' . $review_social . '">Review</a>
                    </div>
                </li>
            ';
        }

        $return = '
        <div class="top-items">
            <div class="item">
                <h3>Top Wallets</h3>
                <ul>
                    ' . $short_code_block . '
                </ul>
            </div>
        ';
        return $return;
    }
} 

new vcWallets();