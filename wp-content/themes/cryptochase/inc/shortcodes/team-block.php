<?php
/**
 * ShortCode Team
**/
class vcTeam extends WPBakeryShortCode {
    public function __construct() {
        add_action('init', array( $this, 'vc_team_mapping' ));
        add_shortcode('vc_team', array( $this, 'vc_team_html' ));
    }

    public function vc_team_mapping() {
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        vc_map(
            array(
                'name'          => __('Team', 'cryptochase'),
                'base'          => 'vc_team',
                'category'      => __('CryptoChase', 'cryptochase'),
                'icon'          =>  '',
                'params'        => array(
                    array(
                        'type'       => 'param_group',
                        'value'      => '',
                        'heading'     => __( 'Team:', 'villavona' ),
                        'param_name' => 'team_slider',
                        'params'     => array(  
                            array(
                                'type'       => 'attach_image',
                                'value'      => '',
                                'heading'    => 'Photo:',
                                'param_name' => 'photo_repeater',
                            ),                       
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Full Name:',
                                'param_name' => 'full_name_repeater',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Position:',
                                'param_name' => 'position_repeater',
                            ),            
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Telegram:',
                                'param_name' => 'telegram_social',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Facebook:',
                                'param_name' => 'facebook_social',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Twitter:',
                                'param_name' => 'twitter_social',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Youtube:',
                                'param_name' => 'youtube_social',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Instagram:',
                                'param_name' => 'instagram_social',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Medium:',
                                'param_name' => 'medium_social',
                            ),
                        )
                    )
                ),
            )
        );
    }

    public function vc_team_html($atts, $content) {
        $team_slider = vc_param_group_parse_atts($atts["team_slider"]);

        foreach ($team_slider as $slider) {
            $full_name_repeater = $slider['full_name_repeater'];
            $position_repeater  = $slider['position_repeater'];
            $photo_repeater     = wp_get_attachment_image_src($slider['photo_repeater']);

            $telegram_social    = $slider['telegram_social'];
            $facebook_social    = $slider['facebook_social'];
            $twitter_social     = $slider['twitter_social'];
            $youtube_social     = $slider['youtube_social'];
            $instagram_social   = $slider['instagram_social'];            
            $medium_social      = $slider['medium_social'];            

            $short_code_block .= '
            <div class="item">
                <img src="' . $photo_repeater[0] . '" srcset="' . get_stylesheet_directory_uri() . '/img/rectangle@2x.jpg 2x, ' . get_stylesheet_directory_uri() . '/img/rectangle@3x.jpg 3x" alt="#">
                <div class="desc">
                    <h5>' . $full_name_repeater . '</h5>
                    <p>' . $position_repeater . '</p>
                    <ul class="soc">';
                        if(!empty($telegram_social)) {
                            $short_code_block .= '<li><a class="icon-telegram" href="' . $telegram_social . '"></a></li>';
                        }
                        if(!empty($facebook_social)) {
                            $short_code_block .= '<li><a class="icon-facebook" href="' . $facebook_social . '"></a></li>';
                        }
                        if(!empty($twitter_social)) {
                            $short_code_block .= '<li><a class="icon-twitter" href="' . $twitter_social . '"></a></li>';
                        }
                        if(!empty($youtube_social)) {
                            $short_code_block .= '<li><a class="icon-youtube" href="' . $youtube_social . '"></a></li>';
                        }
                        if(!empty($instagram_social)) {
                            $short_code_block .= '<li><a class="icon-instagram" href="' . $instagram_social . '"></a></li>';
                        }
                        if(!empty($medium_social)) {
                            $short_code_block .= '<li><a class="icon-medium" href="' . $medium_social . '"></a></li>';
                        }
            $short_code_block .= ' </ul>
                </div>
            </div>
            ';
        }

        $return = '
        <div class="tab-pane" id="team">
            <h3>Project Team</h3>
            <div class="items-teams">
                ' . $short_code_block . '
            </div>
        </div>
        ';
        return $return;
    }
} 

new vcTeam();