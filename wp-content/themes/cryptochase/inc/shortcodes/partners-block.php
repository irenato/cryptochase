<?php
/**
 * ShortCode Partners
**/
class vcPartners extends WPBakeryShortCode {
    public function __construct() {
        add_action('init', array( $this, 'vc_partners_mapping' ));
        add_shortcode('vc_partners', array( $this, 'vc_partners_html' ));
    }

    public function vc_partners_mapping() {
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        vc_map(
            array(
                'name'          => __('Partners', 'cryptochase'),
                'base'          => 'vc_partners',
                'category'      => __('CryptoChase', 'cryptochase'),
                'icon'          =>  '',
                'params'        => array(
                    array(
                        'type'       => 'param_group',
                        'value'      => '',
                        'heading'     => __( 'Partners:', 'villavona' ),
                        'param_name' => 'partners_slider',
                        'params'     => array(  
                            array(
                                'type'       => 'attach_image',
                                'value'      => '',
                                'heading'    => 'Image:',
                                'param_name' => 'Image_repeater',
                            ),                       
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'URL:',
                                'param_name' => 'url_repeater',
                            ),
                        )
                    )
                ),
            )
        );
    }

    public function vc_partners_html($atts, $content) {
        $partners_slider = vc_param_group_parse_atts($atts["partners_slider"]);

        foreach ($partners_slider as $slider) {
            $url_repeater   = $slider['url_repeater'];
            $photo_repeater = wp_get_attachment_image_src($slider['Image_repeater'], array(365,150));

            $short_code_block .= '
                <a class="item" href="' . $url_repeater . '"><img src="' . $photo_repeater[0] . '" alt="#"></a>
            ';
        }

        $return = '
            <div class="tab-pane" id="white-paper"> </div>
            <div class="tab-pane" id="graph"> </div>
            <div class="tab-pane" id="partners">
                <div class="items-partners">
                    ' . $short_code_block . '
                </div>
            </div>
        </div>
        ';
        return $return;
    }
} 

new vcPartners();