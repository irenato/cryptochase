<?php
/**
 * ShortCode Top Exchanges
**/
class vcExchanges extends WPBakeryShortCode {
    public function __construct() {
        add_action('init', array( $this, 'vc_exchanges_mapping' ));
        add_shortcode('vc_exchanges', array( $this, 'vc_exchanges_html' ));
    }

    public function vc_exchanges_mapping() {
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        vc_map(
            array(
                'name'          => __('Top Exchanges', 'cryptochase'),
                'base'          => 'vc_exchanges',
                'category'      => __('CryptoChase', 'cryptochase'),
                'icon'          =>  '',
                'params'        => array(
                    array(
                        'type'       => 'param_group',
                        'value'      => '',
                        'heading'     => __( 'Top Exchanges:', 'villavona' ),
                        'param_name' => 'exchanges_block',
                        'params'     => array(  
                            array(
                                'type'       => 'attach_image',
                                'value'      => '',
                                'heading'    => 'Logo:',
                                'param_name' => 'logo_repeater',
                            ),                       
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Name:',
                                'param_name' => 'name_repeater',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Rating:',
                                'param_name' => 'rating_repeater',
                            ),            
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Buy Now URL:',
                                'param_name' => 'buy_now_social',
                            ),
                            array(
                                'type'       => 'textfield',
                                'value'      => '',
                                'heading'    => 'Review URL:',
                                'param_name' => 'review_social',
                            ),
                        )
                    )
                ),
            )
        );
    }

    public function vc_exchanges_html($atts, $content) {
        $exchanges_block = vc_param_group_parse_atts($atts["exchanges_block"]);
        $how_to_buy_guide   = get_field("how_to_buy_guide"); 
        foreach ($exchanges_block as $exchange) {
            $photo_repeater     = wp_get_attachment_image_src($exchange['logo_repeater']);
            $name_repeater      = $exchange['name_repeater'];
            $rating_repeater    = $exchange['rating_repeater'];
            $buy_now_social     = $exchange['buy_now_social'];
            $review_social      = $exchange['review_social'];


            $short_code_block .= '
                <li>
                    <div class="img"><img src="' . $photo_repeater[0] . '" alt="#"></div>
                    <div class="desc">
                        <h5>' . $name_repeater . '<span class="icon-star">' . $rating_repeater . ' </span></h5>
                    </div>
                    <div class="link">
                        <a class="button" href="' . $buy_now_social . '">Buy Now</a>
                        <a class="review" href="' . $review_social . '">Review</a>
                    </div>
                </li>
            ';
        }

        $return = '
            <div class="item">
                <h3>Top Exchanges</h3>
                <ul>
                    ' . $short_code_block . '
                </ul>
            </div>
        </div>
        ';
        return $return;
    }
} 

new vcExchanges();