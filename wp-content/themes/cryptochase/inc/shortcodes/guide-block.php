<?php
/**
 * ShortCode Guide
**/
class vcGuide extends WPBakeryShortCode {
    public function __construct() {
        add_action('init', array( $this, 'vc_guide_mapping' ));
        add_shortcode('vc_guide', array( $this, 'vc_guide_html' ));
    }

    public function vc_guide_mapping() {
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        vc_map(
            array(
                'name'          => __('Guide', 'cryptochase'),
                'base'          => 'vc_guide',
                'category'      => __('CryptoChase', 'cryptochase'),
                'icon'          =>  '',
                'params'        => array(
                    array(
                        'type'        => 'textarea_html',
                        'value'       => '',
                        'heading'     => __( 'Description:', 'villavona' ),
                        'param_name'  => 'content',
                    ),
                ),
            )
        );
    }

    public function vc_guide_html($atts, $content) {
        $avatar             = get_simple_local_avatar(get_the_author_meta('ID'), 120);
        $user_description   = get_the_author_meta('description');
        $user_nickname      = get_the_author_meta('nickname');
        $date_post          = get_the_date('F j, Y');

        $attachment_image   = get_the_post_thumbnail_url(get_the_ID(), array(32, 32));

        $exchange_coin      = get_field("exchange_coin");
        $negative_coin      = get_field("negative_coin");

        $coins_index        = get_field('coins_index');

        global $post;
        $post_slug          = $post->post_name;
        $post_slug          = ucfirst($post_slug); 
        
        $return = '
        <a class="button transparent" href="' . $how_to_buy_guide . ' ">How to buy guide</a>
        <ul class="nav-tabs">
            <li class="active"><a href="#guide" data-toggle="tab">Guide</a></li>
            <li><a href="#team" data-toggle="tab">Team</a></li>
            <li><a href="#white-paper" data-toggle="tab">White paper</a></li>
            <li><a href="#graph" data-toggle="tab">Graph</a></li>
            <li><a href="#partners" data-toggle="tab">Partners</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="guide">
                <div class="after-data">
                    <div class="aftre-img">' . $avatar . '</div>
                    <p class="desc">By <span>' . $user_nickname . '</span></p>
                    <p class="date">' . $date_post . '</p>
                </div>
                <h3>' . the_title() . '</h3>
                <div class="post-info">
                    <!-- <a class="button icon-tag" href="/coins/">Coins</a> -->
                    <p class="total-views"> <span>' . getPostViews(get_the_ID()) . '</span>Total views </p>
                    <p class="comments" style="display:none;"> <span>No</span>comments</p>
                </div>
                <div class="items-coins">
                    <div class="item">
                        <div class="img"><img src="' . $attachment_image . '" alt="#"></div>
                        <p class="name">' . $post_slug . '</p> 
                        <p class="price">$' . $exchange_coin . '</p>
                        <p class="meta">' . $coins_index . '<span class="icon-arrow-top">' . $negative_coin . '</span></p>
                    </div>
                </div>
                <div class="contents">
                    <h4 class="icon-arrow-bottom">Contents</h4>
                    <ol>
                        
                    </ol>
                </div>
                <div class="content_block">
                    ' . $content . '
                    <h3>About The Author</h3>
                    <div class="after">
                        ' . $avatar . '
                        <div class="desc">
                            <h5>' . $user_nickname . '</h5>
                            <p><em>' . $user_description . '</em></p>
                        </div>
                    </div>
                </div>
            </div>
        ';
        return $return;
    }
} 

new vcGuide();