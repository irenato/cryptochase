<?php


function showUserPtsData($user)
{
    set_query_var('user', $user);
    set_query_var('ptsData', getUserPartnersData($user->ID));
    echo get_template_part('admin-template-parts/section-profile');
}

add_action('edit_user_profile', 'showUserPtsData');
add_action('show_user_profile', 'showUserPtsData');

function showPromoData()
{
    global $post;
    if ($post->post_type === 'promos') {
        echo get_template_part('admin-template-parts/section-promos');
    }
}

add_action('edit_form_advanced', 'showPromoData');

function importRulez($post_ID)
{
    global $wpdb;
    $filedata = get_field('import_prizes_structure', $post_ID);
    $query = "INSERT INTO " . $wpdb->prefix . "promo_rules (`promo_id`, `position`, `prize`) VALUES ";
    $values = [];
    $placeHolders = [];
    if ($filedata) {
        $handle = fopen(get_attached_file($filedata['id']), "r");
        while ($data = fgetcsv($handle, 1000, ",")) {
            if (is_numeric($data[0]) && is_numeric($data[1])) {
                array_push($values, $post_ID, $data[0], $data[1]);
                array_push($placeHolders, "('%d', '%d', '%f')");
            }
        }
        fclose($handle);
        if ($values) {
            $wpdb->query("DELETE FROM " . $wpdb->prefix . "promo_rules WHERE promo_id=" . (int)$post_ID);
            delete_field('import_prizes_structure', $post_ID);
            $query .= implode(',', $placeHolders);
            $wpdb->query($wpdb->prepare("$query ", $values));
            setOption(
                'importReport',
                'Import finished successfully!'
            );
        }
    }
}

add_filter('acf/save_post', 'importRulez', 15, 3);

function importPoints($postId)
{
    global $wpdb;
    $filedata = get_field('import_daily_reports', $postId);
    $platformId = findPostId($filedata['title']);
    if ($filedata && $platformId) {
        logPlatformPts($postId, $platformId);
        $handle = fopen(get_attached_file($filedata['id']), "r");
        while ($data = fgetcsv($handle, 1000, ",")) {
            if (is_numeric($data[1])) {
                $userId = findUserId($data[0], $platformId);
                if ($userId) {
                    $pts = $data[1] * 7;
                    $date = date('Y-m') . '-01';
                    $wpdb->query("INSERT INTO " . $wpdb->prefix . "users_promos
                                SET promo_id = " . esc_sql($postId) . " ,
                                user_platform_id = {$userId},
                                pts = " . $pts . ",
                                created_at = '" . $date . "'
                                ON DUPLICATE KEY UPDATE
                                pts = pts + " . $pts);
                }
            }
        }
        fclose($handle);
        if (isset($pts)) {
            delete_field('import_prizes_structure', $postId);
            setOption(
                'importReport',
                'Import finished successfully!'
            );
        }
    }
}

add_filter('acf/save_post', 'importPoints', 15, 3);

function showUpdateReport($promoId, $startDate, $endDate)
{
    global $wpdb;
    return $wpdb->get_results("SELECT *                                   
                        FROM `" . $wpdb->prefix . "platform_imports_log`                     
                        WHERE promo_id = " . esc_sql($promoId) . "
                        AND created_at >= '" . esc_sql($startDate) . "'                      
                        AND created_at <= '" . esc_sql($endDate) . "'                      
                        ORDER BY platform_id ASC", ARRAY_A);
}

function logPlatformPts($promoId, $platformId)
{
    global $wpdb;
    $wpdb->query("INSERT INTO " . $wpdb->prefix . "platform_imports_log
                                SET promo_id = " . (int)$promoId . ",
                                platform_id = '" . (int)$platformId . "',
                                created_at = '" . date('Y-m-d') . "'
                                ON DUPLICATE KEY UPDATE
                                created_at = '" . date('Y-m-d') . "'");
}


function setOption(string $key, string $value = '')
{
    if (get_option($key)) {
        update_option($key, $value);
    } else {
        add_option($key, $value);
    }
}

add_action('admin_notices', 'showImportNotice');

function showImportNotice()
{
    $optionData = get_option('importReport');
    if ($optionData) {
        delete_option('importReport');
        echo '<div class="notice notice-success is-dismissible">'
            . '<h1>' . $optionData . '</h1>'
            . '</div>';
    }
}

function findUserId($userName, $platformId)
{
    global $wpdb;
    return $wpdb->get_var("SELECT id                 
                        FROM `" . $wpdb->prefix . "users_partners`    
                        WHERE username = '" . esc_sql($userName) . "'                 
                        AND partner_id = '" . esc_sql($platformId) . "'                             
                        LIMIT 1");
}

function findPostId($value)
{
    global $wpdb;
    $value = strtolower($value);
    return $wpdb->get_var("SELECT ID                 
                        FROM `" . $wpdb->prefix . "posts`    
                        WHERE post_type = 'platforms'                 
                        AND LOWER(post_title) LIKE  '%" . esc_sql($value) . "%'                    
                        LIMIT 1");
}

function getPromoRules($promoId, $limit = 0)
{
    global $wpdb;
    return $wpdb->get_results("SELECT position as position, 
                        prize as prize                                     
                        FROM `" . $wpdb->prefix . "promo_rules`                     
                        WHERE promo_id = " . esc_sql($promoId) . "     
                        AND prize > 0                 
                        ORDER BY position ASC 
                        " . ($limit ? " LIMIT " . $limit : ''), ARRAY_A);
}

function getPromoWinners($promoId, $limit)
{
    global $wpdb;
    return $wpdb->get_results("SELECT 
                   SUM(up.pts) as pts,
                        GROUP_CONCAT(DISTINCT up2.partner_id ORDER BY up2.id ASC SEPARATOR ',') as platforms_ids,
                        u.display_name as username                   
                        FROM `wp_users_promos` up
                        INNER JOIN `wp_users_partners` up2
                        ON up.user_platform_id = up2.id
                        INNER JOIN `wp_users` u
                        ON u.ID = up2.user_id
                        WHERE up.promo_id = " . esc_sql($promoId) . "                       
                        GROUP BY u.ID                                  
                        ORDER BY up2.id, up.pts DESC
                        LIMIT {$limit}", ARRAY_A);
}

function preparePromoResults($promoId, $limit = 0)
{
    $rules = getPromoRules($promoId, $limit);
    if ($rules) {
        $winners = getPromoWinners($promoId, count($rules));
        return array_map(function ($key, $rule) use ($winners) {
            if (isset($winners[$key])) {
                $winners[$key]['platforms_ids'] = explode(',', $winners[$key]['platforms_ids']);
            }
            return array_merge($rule, $winners[$key] ?? []);
        }, array_keys($rules), $rules);
    }
}

function getThumbnails($postType = 'platforms', $size = 'thumbnail')
{
    $thumbnails = [];
    $posts = getCustomPosts($postType);
    foreach ($posts as $post) {
        $thumbnails[$post->ID] = get_the_post_thumbnail_url($post->ID, $size);
    }
    return $thumbnails;
}

function updateUserPtsData($user_id)
{
    global $wpdb;
    if (isset($_POST['pts'])) {
        foreach ($_POST['pts'] as $rowId => $values) {
            if ($values['partner_id'] > 0 && $values['username']) {
                $wpdb->query("INSERT INTO " . $wpdb->prefix . "users_partners
                                SET user_id = " . esc_sql($user_id) . " ,
                                partner_id = " . (int)$values['partner_id'] . ",
                                username = '" . esc_sql($values['username']) . "'
                                ON DUPLICATE KEY UPDATE
                                user_id = " . esc_sql($user_id) . " ,
                                partner_id = " . (int)$values['partner_id'] . ",
                                username = '" . esc_sql($values['username']) . "'");
            }
        }
    }
}

add_action('edit_user_profile_update', 'updateUserPtsData');
add_action('personal_options_update', 'updateUserPtsData');

function getCustomPosts(string $customPostType): array
{
    return get_posts(
        [
            'numberposts' => -1,
            'category' => 0,
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => $customPostType,
        ]
    );
}

function getUserPartnersData($userId)
{
    global $wpdb;
    return $wpdb->get_results("SELECT up.id as id, 
                        up.partner_id as partner_id, 
                        up.username as username,                   
                        p.post_title as title
                        FROM `" . $wpdb->prefix . "users_partners` up
                        INNER JOIN `" . $wpdb->prefix . "posts` p
                        ON up.partner_id = p.ID
                        WHERE user_id = " . esc_sql($userId) . "                      
                        ORDER BY updated_at DESC");
}

function getDates()
{
    $currentYear = date('Y');
    return [
        'months' => getMonths(),
        'minYear' => $currentYear - 1,
        'maxYear' => $currentYear + 2,
    ];

}

add_action('wp_ajax_ajaxShowPromoData', 'ajaxShowPromoData');
add_action('wp_ajax_nopriv_ajaxShowPromoData', 'ajaxShowPromoData');
function ajaxShowPromoData()
{

    wp_send_json(getPromoData($_POST['date']));
}

add_action('wp_ajax_ajaxShowContent', 'ajaxShowContent');
add_action('wp_ajax_nopriv_ajaxShowContent', 'ajaxShowContent');

function ajaxShowContent()
{

    wp_send_json(get_template_part('templates/content-promos'));
}

function getMonths()
{
    return [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ];
}

function getPromoData($date)
{
    global $wpdb;
    $range = [
        $date . '-01 00:00:00',
        $date . '-31 00:00:00',
    ];
    return $wpdb->get_row("
        SELECT ROUND(SUM(prize), 2) as prizes,
        COUNT(id) as amount
        FROM " . $wpdb->prefix . "promo_rules
        WHERE promo_id
        IN(
        SELECT pm.post_id FROM " . $wpdb->prefix . "postmeta pm
        INNER JOIN " . $wpdb->prefix . "postmeta pm1
        ON pm.post_id = pm1.post_id
        WHERE (pm.meta_key = 'start_date' AND pm.meta_value <= '" . $range[0] . "')
        AND prize > 0
        AND pm1.meta_key = 'end_date' AND pm1.meta_value >= '" . $range[1] . "')", ARRAY_A);
}

function getGraphValues()
{
    global $wpdb;
    $data = $wpdb->get_results("
    SELECT up.created_at as year, 
    SUM(up.pts) as points 
    FROM " . $wpdb->prefix . "users_promos up 
    INNER JOIN " . $wpdb->prefix . "users_partners upr
    ON up.user_platform_id = upr.id
    WHERE upr.user_id=" . (int)get_current_user_id() . " 
    GROUP BY up.created_at", ARRAY_A);
    $months = getMonths();

    return json_encode(array_map(function ($item) use ($months) {
        $dates = explode('-', $item['year']);
        $item['year'] = $months[$dates[1]] . ' (' . $dates[0] . ')';
        $item['points'] = (int)$item['points'];
        return $item;
    }, $data));
}


add_action('wp_ajax_ajaxRemoveSubUser', 'ajaxRemoveSubUser');
function ajaxRemoveSubUser()
{
    global $wpdb;
    wp_send_json($wpdb->query(
        "DELETE FROM " . $wpdb->prefix . "users_partners WHERE id= " . (int)$_POST['userId']
    ));
}

