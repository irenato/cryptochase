<?php 
/* 
* Template Name: Account
*/ 

if ( is_user_logged_in() ) { 
    
} else { 
    $redirect = home_url();
    wp_safe_redirect($redirect);
    exit; 
}

get_header(); 
$cur_user_id = get_current_user_id();
$user_nickname = get_the_author_meta('nickname', $cur_user_id); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="account">
                    <div class="content">
                        <h1>Account Area</h1>
                        <?php
                            $args = array( 
                                'post_type'      => 'promos', 
                                'posts_per_page' => 1,
                                'category_name'  => 'generic-promo'
                            );
                            $loop = new WP_Query( $args );
                            while ( $loop->have_posts() ) : $loop->the_post(); 
                                $attachment_image   = get_the_post_thumbnail_url(get_the_ID()); ?>
                                <a href="<?php echo get_post_permalink(); ?>" class="promos">
                                    <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                                    <div class="description">
                                        <h2><?php echo get_the_title(); ?></h2>
                                        <div class="block-clock">
                                            <p>Time left:</p>
                                            <div class="clock-1" data-date="<?php the_field('end_date'); ?>"></div>
                                        </div>
                                    </div>
                                    <div class="table four-el">
                                        <?php the_field('table_generic'); ?>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        <div class="bind-block">
                            <h2>Make Sure You’re Credited CryptoChase Points<span>Bind Your Crypto Platform Accounts To CryptoChase</span></h2>
                            <?php
                            $args = array( 
                                'post_type'      => 'platforms', 
                                'posts_per_page' => -1,
                            );
                            $loop = new WP_Query( $args );
                            while ( $loop->have_posts() ) : $loop->the_post(); 
                                $attachment_image   = get_the_post_thumbnail_url(get_the_ID()); ?>
                                <div class="item">
                                    <div class="img"><img class="image_platform" src="<?php echo $attachment_image; ?>" data-id="<?= get_the_ID() ?>" alt="#" data-name="<?php echo get_the_title(); ?>"></div>
                                    <div class="input">
                                        <input class="username username_account" type="text" placeholder="Enter Identifier. Click ‘i’ for more info">
                                        <div class="tooltip icon-info" data-toggle="modal" data-target=".info-modal-<?php echo get_the_ID(); ?>"></div>
                                    </div>
                                    <a class="button send_platform" href="#">Bind</a>
                                </div>

                                <div class="modal fade info-modal info-modal-<?php echo get_the_ID(); ?>" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <a class="icon-close" data-dismiss="modal" aria-hidden="true"></a>
                                            <?php the_field('rules'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="sitebar">
                        <p class="welcome">Welcome: <?php echo $user_nickname; ?><span></span></p>
                        <div class="bound">
                            <h4>Bound Accounts</h4>
                            <?php
                            $ptsData = getUserPartnersData(get_the_author_meta('ID'));
                            if (!empty($ptsData)): ?>
                                <ul>
                                    <?php foreach ($ptsData as $ptsDatum):  ?>
                                        <li>
                                            <div class="img"><img src="<?= get_the_post_thumbnail_url($ptsDatum->partner_id); ?>" alt="<?= $ptsDatum->username ?>"></div>
                                            <p><?= $ptsDatum->username ?></p>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="history-block">
                    <img class="woman" src="<?php echo get_stylesheet_directory_uri(); ?>/img/girl.png" alt="#">
                    <h2>Your Historical CryptoChase Points Data</h2>
                    <div id="chartdiv" data-values='<?= getGraphValues(); ?>'></div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade alert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h1>Please, use only letters and numbers, without spaces, max 15 chars length.</h1>
            <h1 style="display:none;">This username was already bound.</h1>
            <h1 style="display:none;">Thanks! Platform username was successfully bound.</h1>
            <a class="button" href="#" data-dismiss="modal" aria-hidden="true">Ok</a>
        </div>
    </div>
</div>

<?php get_footer(); ?>