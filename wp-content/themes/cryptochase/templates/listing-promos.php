<?php
/* 
* Template Name: Promos
*/

get_header();
$promoData = getPromoData(date('Y-m'));
?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="account">
                        <div class="content">
                            <div class="slider-date js-months">
                                <?php $dates = getDates();
                                while ($dates['minYear'] <= $dates['maxYear']):
                                    foreach ($dates['months'] as $key => $month):
                                        ?>
                                        <div class="item"><a href="#" class="js-month"
                                                             data-date="<?= $dates['minYear'] . '-' . $key ?>"><?= $month ?>
                                                <span><?= $dates['minYear'] ?></span></a></div>
                                    <?php
                                    endforeach;
                                    ++$dates['minYear'];
                                endwhile;
                                ?>
                            </div>
                            <span>CryptoChase brings you the biggest and best cryptocurrency promotions, giveaways & airdrops in the world. Sign up to a CryptoChase partner platform and get rewarded for trading or gambling crypto.</span>
                            <div class="js-content-inner">
                                <?php get_template_part('templates/content-promos') ?>
                            </div>
                        </div>

                        <div class="sitebar">
                            <div class="total-price">
                                <p>TOTAL PRIZES ($)<span class="js-report-prise">$<?= $promoData['prizes'] ?></span></p>
                            </div>
                            <div class="avalilable-prizes">
                                <p>&#35; AVAILABLE PRIZES <span
                                            class="js-report-amount"><?= $promoData['amount'] ?></span></p>
                            </div>
                            <h3>Chase Crypto On Twitter</h3>
                            <?php echo do_shortcode('[custom-twitter-feeds]'); ?>
                            <?php if (is_active_sidebar('information_widget')) : ?>
                                <?php dynamic_sidebar('information_widget'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>