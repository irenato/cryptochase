<?php 
/* 
* Template Name: Coins
*/ 

get_header(); 

$total_market_cap = get_field("market_сap", 'information_crypto');
$total_volume_24h = get_field("24h_volume", 'information_crypto');
$btc_dominance    = get_field("btc_dominance", 'information_crypto'); ?>

<section>
    <div class="table-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Coins</h2>
                    <p>Detailed information on each and every cryptocurrency.</p>
                    <ul class="description">
                        <li><strong>Market Сap:</strong>$<?php echo $total_market_cap; ?></li>
                        <li><strong>24H Volume:</strong>$<?php echo $total_volume_24h; ?></li>
                        <li><strong>BTC Dominance:</strong><?php echo $btc_dominance; ?>%</li>
                    </ul>
                    <div class="table coins">
                        <div class="theader">
                            <div class="tr">
                                <div class="th">Name</div>
                                <div class="th">Description</div>
                                <div class="th"> <a class="icon-arrow" href="#">Market cap</a></div>
                                <div class="th"> <a class="icon-arrow" href="#">24H VOL</a></div>
                                <div class="th">Top Exchanges</div>
                                <div class="th"> </div>
                            </div>
                        </div>
                        <div class="tbody tbody-dedesktop">
                            <?php
                            $args = array( 
                                'post_type'      => 'coin', 
                                'posts_per_page' => -1 
                            );
                            $loop = new WP_Query( $args );
                            while ( $loop->have_posts() ) : $loop->the_post(); 
                                global $post;
                                $post_slug = $post->post_name;
                                $post_slug = ucfirst($post_slug); 
                                
                                $attachment_image   = get_the_post_thumbnail_url(get_the_ID(), array(30, 30)); 
                                $coins_index = get_field('coins_index');

                                $exchange_coin    = get_field("exchange_coin");
                                $market_cap_coin  = get_field("market_cap_coin");
                                $h_vol_coin       = get_field("h_vol_coin");
                                $negative_coin    = get_field("negative_coin"); ?>
                                <div class="tr">
                                    <div class="td" data-title="Name">
                                        <a class="img" href="<?php echo get_post_permalink(); ?>">
                                            <img src="<?php echo $attachment_image ?>" alt="#">
                                        </a>
                                        <a href="<?php echo get_post_permalink(); ?>">
                                            <div class="desc">
                                                <p><?php echo $post_slug; ?><strong>$<?php echo $exchange_coin; ?></strong><?php echo $coins_index; ?> <span><?php echo $negative_coin; ?></span></p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="td" data-title="Description">
                                        <a href="<?php echo get_post_permalink(); ?>">
                                            <?php the_excerpt(); ?>
                                        </a>
                                    </div>
                                    <div class="td" data-title="Market cap">$<?php echo $market_cap_coin; ?></div>
                                    <div class="td" data-title="24H VOL">$<?php echo $h_vol_coin; ?></div>
                                    <div class="td" data-title="Top Exchanges"> 
                                        <?php if( have_rows('top_exchanges') ): ?>
                                            <?php while( have_rows('top_exchanges') ): the_row(); 
                                                $logo_exchanges        = get_sub_field('logo_exchanges');
                                                $name_exchanges        = get_sub_field('name_exchanges');
                                                $rating_exchanges      = get_sub_field('rating_exchanges');
                                                $buy_now_url_exchanges = get_sub_field('buy_now_url_exchanges');
                                                $review_url_exchanges  = get_sub_field('review_url_exchanges'); ?>
                                                <div class="img">
                                                    <img src="<?php echo $logo_exchanges['url']; ?>" alt="#">
                                                </div>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="td" data-title="">
                                        <a class="button" href="<?php the_field('buy_now_url'); ?>">Buy Now</a>
                                        <a class="review" href="<?php the_field('review_url'); ?>">Review</a>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="table coins-mobile">
                        <div class="theader">
                            <div class="tr">
                                <div class="th">Name</div>
                                <div class="th">Price</div>
                                <div class="th">Change</div>
                                <div class="th"></div>
                            </div>
                        </div>
                        <div class="tbody">
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                            <div class="tr">
                                <div class="td" data-title="Name">
                                    <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></a>
                                    <div class="desc">
                                        <p>Bitcoin</p>
                                    </div>
                                </div>
                                <div class="td" data-title="Price">
                                    <p><strong>$7,260.51 </strong>BTC </p>
                                </div>
                                <div class="td" data-title="Change">
                                    <p><strong>-0.94%</strong></p>
                                </div>
                                <div class="td" data-title=""><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>