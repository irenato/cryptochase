<?php
 $date = $_POST['date'] ?? date('Y-m');
?>

<?php
$args = [
    'post_type' => 'promos',
    'posts_per_page' => -1,
    'meta_query' => [
        [
            'key' => 'start_date',
            'value' => $date . '-01 00:00:00',
            'compare' => '<=',
            'type' => 'DATE'
        ],
        [
            'key' => 'end_date',
            'value' => $date . '-31 00:00:00',
            'compare' => '>=',
            'type' => 'DATE'
        ],
        'relation' => 'AND',
    ]
];
$loop = new WP_Query($args);
while ($loop->have_posts()) : $loop->the_post();
    $attachment_image = get_the_post_thumbnail_url(get_the_ID());
    if (in_category('generic-promo')) { ?>
        <a href="<?php echo get_post_permalink(); ?>" class="promos">
            <div class="banner"
                 style="background-image: url(<?php echo $attachment_image; ?>);"></div>
            <div class="description">
                <h2><?php echo get_the_title(); ?></h2>
                <div class="block-clock">
                    <p>Time left:</p>
                    <div class="clock-1" data-date="<?php the_field('end_date'); ?>"></div>
                </div>
            </div>
            <div class="table four-el">
                <?php the_field('table_generic'); ?>
            </div>
        </a>
    <?php } elseif (in_category('standard-promo')) {
        $platforms = get_field('platforms'); ?>
        <a href="<?php echo get_post_permalink(); ?>" class="promos">
            <div class="banner"
                 style="background-image: url(<?php echo $attachment_image; ?>);"></div>
            <div class="description">
                <h2><?php echo get_the_title(); ?></h2>
                <div class="block-clock">
                    <p>Time left:</p>
                    <div class="clock-1" data-date="<?php the_field('end_date'); ?>"></div>
                </div>
                <p>Platforms: <strong>
                        <?php
                        foreach ($platforms as $platform):
                            echo $platform->post_title;
                            echo ', ';
                        endforeach;
                        ?>
                    </strong></p>
            </div>
            <?php $winners = preparePromoResults(get_the_ID(), 5); ?>
            <div class="table four-el">
                <div class="theader">
                    <div class="tr">
                        <div class="th" data-title="Pos">Pos</div>
                        <div class="th" data-title="Prize ($)">Prize ($)</div>
                        <div class="th" data-title="User">User</div>
                        <div class="th" data-title="Pts">Pts</div>
                    </div>
                </div>
                <div class="tbody">
                    <?php if ($winners):
                        foreach ($winners as $key => $winner): ?>
                            <div class="tr">
                                <div class="td" data-title="Pos">#<?= ++$key ?></div>
                                <div class="td" data-title="Prize ($)">
                                    $ <?= $winner['prize'] ?></div>
                                <div class="td"
                                     data-title="User"><?= $winner['username'] ?? ' - ' ?></div>
                                <div class="td"
                                     data-title="Pts"><?= $winner['pts'] ?? ' - ' ?></div>
                            </div>
                        <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </a>
    <?php } ?>
<?php endwhile; ?>