<?php 
/* 
* Template Name: Coin
*/ 

get_header(); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="bread-crumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Coins</a></li>
                    <li><a href="#">Bitcoin Ultimate Guide – Everything you need to know</a></li>
                </ul>
                <div class="coin">
                    <div class="content">
                        <p class="info"><strong>$</strong><i class="icon-arrow-top">%</i></p>
                        <div class="soc">
                            <ul>
                                <li><a class="icon-facebook" href="#"> </a></li>
                                <li><a class="icon-twitter" href="#"> </a></li>
                                <li><a class="icon-telegram" href="#"></a></li>
                                <li><a class="icon-reddit" href="#"> </a></li>
                            </ul>
                            <a href="#">https://bitcoin.org</a>
                        </div>
                        <div class="clearfix"></div>
                        <ul class="description">
                            <li><strong>Market Сap:</strong>$0</li>
                            <li><strong>Coin Supply:</strong>0</li>
                            <li><strong>24H Vol:</strong>$0</li>
                        </ul>
                        <div class="top-items">
                            <div class="item">
                                <h3>Top Wallets</h3>
                                <ul>
                                    <li>
                                        <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/ledger-wallet.png" alt="#"></div>
                                        <div class="desc">
                                            <h5>Ledger Wallet<span class="icon-star">9.8 </span></h5>
                                        </div>
                                        <div class="link"><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                                    </li>
                                    <li>
                                        <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/trezor-wallet.png" alt="#"></div>
                                        <div class="desc">
                                            <h5>Trezor Wallet<span class="icon-star">9.8 </span></h5>
                                        </div>
                                        <div class="link"><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                                    </li>
                                    <li>
                                        <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/exodus-wallet.png" alt="#"></div>
                                        <div class="desc">
                                            <h5>Exodus Wallet<span class="icon-star">9.8 </span></h5>
                                        </div>
                                        <div class="link"><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <h3>Top Exchanges</h3>
                                <ul>
                                    <li>
                                        <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/coinbase.png" alt="#"></div>
                                        <div class="desc">
                                            <h5>Coinbase<span class="icon-star">9.8 </span></h5>
                                        </div>
                                        <div class="link"><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                                    </li>
                                    <li>
                                        <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/etoro.png" alt="#"></div>
                                        <div class="desc">
                                            <h5>eToro<span class="icon-star">9.8 </span></h5>
                                        </div>
                                        <div class="link"><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                                    </li>
                                    <li>
                                        <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/plus500.png" alt="#"></div>
                                        <div class="desc">
                                            <h5>Plus500<span class="icon-star">9.8 </span></h5>
                                        </div>
                                        <div class="link"><a class="button" href="#">Buy Now</a><a class="review" href="#">Review</a></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <a class="button transparent" href="#">How to buy guide</a>
                        <ul class="nav-tabs">
                            <li class="active"><a href="#guide" data-toggle="tab">Guide</a></li>
                            <li><a href="#teamt" data-toggle="tab">Team</a></li>
                            <li><a href="#white-paper" data-toggle="tab">White paper</a></li>
                            <li><a href="#graph" data-toggle="tab">Graph</a></li>
                            <li><a href="#partners" data-toggle="tab">Partners</a></li>
                        </ul>
                        <!-- Tab panes-->
                        <div class="tab-content">
                            <div class="tab-pane active" id="guide">
                                <div class="after-data">
                                    <div class="aftre-img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/after.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/after@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/after@3x.jpg 3x" alt="#"></div>
                                    <p class="desc">By <span>Tom Alford</span></p>
                                    <p class="date">FEB 18, 2018</p>
                                </div>
                                <h3>Bitcoin Ultimate Guide – Everything you need to know</h3>
                                <div class="post-info">
                                    <!-- <a class="button icon-tag" href="/coins/">Coins</a> -->
                                    <p class="total-views"> <span>1109</span>Total views </p>
                                    <p class="comments"> <span>No</span>comments</p>
                                </div>
                                <div class="items-coins">
                                    <div class="item">
                                        <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/coins/bitcoin.png" alt="#"></div>
                                        <p class="name">Bitcoin</p>
                                        <p class="price">$8,062.60</p>
                                        <p class="meta">BTC<span class="icon-arrow-top">10.36%</span></p>
                                    </div>
                                </div>
                                <div class="contents">
                                    <h4 class="icon-arrow-bottom">Contents</h4>
                                    <ol>
                                        
                                    </ol>
                                </div>
                                <div class="content_block">
                                    <h3>Bitcoin Summary:</h3>
                                    <ul>
                                        <li>Bitcoin is the original and most well known cryptocurrency. As a result, Bitcoin is usually the go to crypto for new investors.</li>
                                        <li>New Bitcoins are created by computers solving problems in a process called a mining.</li>
                                        <li>Limited supply: Only 21 million Bitcoins will ever be created. This means as demand increases the price of bitcoin will increase as well.</li>
                                        <li>Divisibility: Each Bitcoin can be broken down into 100 million units called satoshis. This means you don’t have to buy one whole bitcoin.</li>
                                        <li>Bitcoin uses: Currency.</li>
                                        <li>Bitcoins characteristics such as portability, divisibility and limited supply have led many investors to view it as digital gold.</li>
                                        <li>Famous people involved in Bitcoin include; The Winklevoss twins, Floyd Mayweather, Paris Hilton, Katy Perry and many more.</li>
                                        <li>Cyber security pioneer John McAfee has boldly predicted that a single Bitcoin will be worth $1M by 2020.</li>
                                    </ul>
                                    <div class="img">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/img.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/img@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/img@3x.jpg 3x" alt="#">
                                        <p>John McAfee Strongly Believes Bitcoin Will Hit $1M. We Hope For His Own Sake That It Does!</p>
                                    </div>
                                    <h3>What is Bitcoin?</h3>
                                    <p>Bitcoin rose out of the ashes from the financial crash of 2008 and was created by a developer under the alias Satoshi Nakamoto. The concept was to create a simple means of exchange that was independent from any central authority (e.g. banks) and could be transferred securely and in a verifiable way through cryptography.</p>
                                    <p>Put simply, Bitcoin acts as a store and transfer of value. It’s because of these characteristics that many commentators refer to Bitcoin as digital gold.</p>
                                    <p>Even today, no one knows the true identity of Satoshi Nakamoto.</p>
                                    <h3>Should You Invest In Bitcoin?</h3>
                                    <p>Recently critics have argued that Bitcoin is a poor value transfer due to slow transactions and high transfer fees. However, the implementation of Segwit and the lightning network should solve these transfer issues and enable bitcoin to scale.</p>
                                    <p>Some argue that Bitcoin has no value whatsoever and will say Bitcoin is in a bubble. You have to ask yourself what makes a dollar worth anything? It currently costs around $1,000 in electricity to mine a single bitcoin, but how much does that $100 bill cost to actually make? The Bitcoin network is global, connects millions of people around the world and Bitcoin is being adopted by more people everyday. Maybe Bitcoin is worthless or maybe the establishment has a vested interest in maintaining the status quo?</p>
                                    <p>If you agree with some of the following questions then maybe investing in Bitcoin is not a bad idea… Don’t fully trust your government and the banks? Think that more people will adopt Bitcoin in the future? Do you think the central bank printing presses will eventually lead to currency failure? Are we on the brink of global change? </p>
                                    <h3>About The Author</h3>
                                    <div class="after">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/after.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/after@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/after@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Tom Alford</h5>
                                            <p> <em>Tom is a cryptocurrency expert and investor from Edinburgh, United Kingdom, with over 5 years of experience in the field. He holds an MA in diplomacy and BA in politics from the University of Nottingham, giving him a firm understanding of the social implications and political factors in cryptocurrency. He believes in long-term projects rather than any short term gains, and is a strong advocate of the future application of blockchain technology.Contact Tom:<a href="#">tom@totalcrypto.io</a></em></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="teamt">
                                <h3>Project Team</h3>
                                <div class="items-teams">
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Satoshi Nakamoto</h5>
                                            <p>Founder</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-2.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-2@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-2@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Martti Malmi</h5>
                                            <p>Satoshi's Right-Hand Man</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-9.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-9@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-9@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Will Binns</h5>
                                            <p>Website Maintenance</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-11.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-11@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-11@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Craig Watkins</h5>
                                            <p>Wallet Maintenance</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-12.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-12@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-12@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Ar Vicco</h5>
                                            <p>Russian Translation</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-13.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-13@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-13@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Simon Alexander</h5>
                                            <p>German Translation</p>
                                            <ul class="soc">
                                                <li><a class="icon-telegram" href="#"></a></li>
                                                <li><a class="icon-reddit" href="#"></a></li>
                                                <li><a class="icon-facebook" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/group-20.svg" alt="#">
                                        <div class="desc">
                                            <h5>Jacob Burenstam</h5>
                                            <p>Swedish Translation</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-15.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-15@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-15@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Mihai Onosie</h5>
                                            <p>Romanian Translation</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/group-20.svg" alt="#">
                                        <div class="desc">
                                            <h5>Matija Mazi</h5>
                                            <p>Slovenian Translation</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-16.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-16@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/rectangle-copy-16@3x.jpg 3x" alt="#">
                                        <div class="desc">
                                            <h5>Martti Malmi</h5>
                                            <p>Satoshi's Right-Hand Man</p>
                                            <ul class="soc">
                                                <li><a class="icon-reddit" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="white-paper"> </div>
                            <div class="tab-pane" id="graph"> </div>
                            <div class="tab-pane" id="partners">
                                <div class="items-partners"><a class="item" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Yeecall.jpg" alt="#"></a><a class="item" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Yeecall.jpg" alt="#"></a><a class="item" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Yeecall.jpg" alt="#"></a><a class="item" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Yeecall.jpg" alt="#"></a><a class="item" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Yeecall.jpg" alt="#"></a><a class="item" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Yeecall.jpg" alt="#"></a><a class="item" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Yeecall.jpg" alt="#"></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="sitebar">
                        <h3>Chase Crypto On Twitter</h3>
                        <a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter-com-total-crypto-io-i-phone-x.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter-com-total-crypto-io-i-phone-x@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/twitter-com-total-crypto-io-i-phone-x@3x.jpg 3x" alt="#"></a>
                        <h3>Join Our Top Community</h3>
                        <a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/group-5.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/group-5@2x.png 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/group-5@3x.png 3x" alt="#"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>