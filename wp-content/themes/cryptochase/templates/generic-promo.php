<?php 
/* 
* Template Name: Generic Promo
*/ 

get_header(); 
$attachment_image   = get_the_post_thumbnail_url(get_the_ID()); ?>

<section>
    <div class="promo">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="bread-crumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/listings-promos/">Generic Promos</a></li>
                            <li><a href="#"><?php the_title(); ?></a></li>
                        </ul>
                        <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                        <h2><?php the_title(); ?></h2>
                        <div class="timer_deals">
                            <div class="block-clock">
                                <p>Time left:</p>
                                <div class="clock-1" data-date="<?php the_field('end_date'); ?>"></div>
                            </div>
                            <?php if(have_rows('deals', get_the_ID())): ?>
                                <?php while(have_rows('deals', get_the_ID())): the_row(); 
                                    $title_deals          = get_sub_field('title_deals');
                                    $description_deals    = get_sub_field('description_deals');
                                    $rating_deals         = get_sub_field('rating_deals');
                                    $logo_deals           = get_sub_field('logo_deals');
                                    $sign_up_deals        = get_sub_field('sign_up_deals');
                                    $review_deals         = get_sub_field('review_deals'); ?>
                                    <a href="<?php echo $review_deals; ?>" target="_blank">
                                        <div class="el-coin">
                                            <div class="img"><img src="<?php echo $logo_deals['url']; ?>" alt="#"></div>
                                            <div class="desc">
                                                <h6><?php echo $title_deals; ?><span class="icon-star"><?php echo $rating_deals; ?> </span></h6>
                                                <p><?php echo $description_deals; ?></p>
                                            </div>
                                            <div class="link"><a class="button" href="<?php echo $sign_up_deals; ?>">Sign Up</a><a class="review" href="<?php echo $review_deals; ?>">Review</a></div>
                                        </div>
                                    </a>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <a class="rules icon-arrow-bottom" href="#">rules</a>
                        <div class="rules-description">
                            <?php the_field('rules'); ?>
                        </div>
                        <div class="table four-el">
                            <?php the_field('table_generic'); ?>
                            <img class="woman" src="<?php echo get_stylesheet_directory_uri(); ?>/img/girl.png" alt="#">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="reports"> 
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p class="reports-update" href="#">Last report update 26/04/2019</p>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>