<?php
/* 
* Template Name: Promo
*/

get_header();
$attachment_image = get_the_post_thumbnail_url(get_the_ID());
$platforms = get_field('platforms');
$winners = preparePromoResults(get_the_ID());
$thumbnails = getThumbnails();
?>

    <section>
        <div class="promo">
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul class="bread-crumb">
                                <li><a href="/">Home</a></li>
                                <li><a href="/listings-promos/">Promos</a></li>
                                <li><a href="#"><?php the_title(); ?></a></li>
                            </ul>
                            <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                            <h2><?php the_title(); ?></h2>
                            <div class="block-clock">
                                <p>Time left:</p>
                                <div class="clock-1" data-date="<?php the_field('end_date'); ?>"></div>
                            </div>
                            <h5>Platforms:<strong>
                                    <?php
                                    foreach ($platforms as $platform):
                                        $get_url = get_field('url', $platform->ID);
                                        echo '<a href="' . $get_url . '" target="_blank">' . $platform->post_title . '</a>, ';
                                    endforeach;
                                    ?>
                                </strong></h5>
                            <a class="rules icon-arrow-bottom" href="#">rules</a>
                            <div class="rules-description">
                                <?php the_field('rules'); ?>
                            </div>
                            <div class="table">
                                <div class="theader">
                                    <div class="tr">
                                        <div class="th" data-title="Pos">Pos</div>
                                        <div class="th" data-title="Prize ($)">Prize ($)</div>
                                        <div class="th" data-title="User">User</div>
                                        <div class="th" data-title="Platform"> Platform</div>
                                        <div class="th" data-title="Pts">Pts</div>
                                    </div>
                                </div>
                                <div class="tbody">
                                    <?php
                                    if (isset($winners)):
                                        foreach ($winners as $key => $winner):
                                            ?>

                                            <div class="tr">
                                                <div class="td" data-title="Pos">#<?= ++$key ?></div>
                                                <div class="td" data-title="Prize ($)">$ <?= $winner['prize'] ?></div>
                                                <div class="td"
                                                     data-title="User"><?= $winner['username'] ?? ' - ' ?></div>
                                                <div class="td" data-title="Platform">
                                                    <?php
                                                    if (isset($winner['platforms_ids'])):
                                                        foreach (($winner['platforms_ids']) as $platformId):
                                                            if (isset($thumbnails[$platformId])): ?>
                                                                <a class="img" href="#">
                                                                    <img src="<?= $thumbnails[$platformId] ?>"
                                                                         alt="<?= $winner['username'] ?>"></a>
                                                            <?php
                                                            endif;
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </div>
                                                <div class="td" data-title="Pts"><?= $winner['pts'] ?? ' - ' ?></div>
                                            </div>

                                        <?php endforeach;
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="reports">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="reports-update icon-arrow-bottom"
                                                                        href="#">Last
                        report update</a></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul>
                            <?php $reports = showUpdateReport(
                                $post->ID,
                                get_field('start_date'),
                                get_field('end_date')
                            );
                            if ($reports):
                                foreach ($reports as $report):
                                    ?>
                                    <li>
                                        <a href="#">
                                            <div class="img"><img
                                                        src="<?= $thumbnails[$report['platform_id']] ?>"
                                                        alt="#"></div>
                                            <p><?= str_replace('-', '/', $report['created_at']) ?></p>
                                        </a>
                                    </li>
                                <?php endforeach;
                            endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>