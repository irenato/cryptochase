<?php 
/* 
* Template Name: Promos
*/ 

get_header(); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="account">
                    <div class="content">
                        <div class="slider-date">
                            <div class="item"><a href="#">January <span>2019</span></a></div>
                            <div class="item"><a href="#">February <span>2019</span></a></div>
                            <div class="item"><a href="#">March <span>2019</span></a></div>
                            <div class="item"><a href="#">April <span>2019</span></a></div>
                            <div class="item"><a href="#">May <span>2019</span></a></div>
                            <div class="item"><a href="#">June <span>2019</span></a></div>
                            <div class="item"><a href="#">July <span>2019</span></a></div>
                            <div class="item"><a href="#">August <span>2019</span></a></div>
                            <div class="item"><a href="#">September <span>2019</span></a></div>
                            <div class="item"><a href="#">October <span>2019</span></a></div>
                            <div class="item"><a href="#">November <span>2019</span></a></div>
                            <div class="item"><a href="#">December <span>2019  </span></a></div>
                        </div>
                        <span>CryptoChase brings you the biggest and best cryptocurrency promotions, giveaways & airdrops in the world. Sign up to a CryptoChase partner platform and get rewarded for trading or gambling crypto.</span>
                        <?php
                            $args = array( 
                                'post_type'      => 'promos', 
                                'posts_per_page' => -1 
                            );
                            $loop = new WP_Query( $args );
                            while ( $loop->have_posts() ) : $loop->the_post(); 
                                $attachment_image   = get_the_post_thumbnail_url(get_the_ID()); 
                                if (in_category('generic-promo')) { ?>
                                    <a href="<?php echo get_post_permalink(); ?>" class="promos">
                                        <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                                        <div class="description">
                                            <h2><?php echo get_the_title(); ?></h2>
                                            <div class="block-clock">
                                                <p>Time left:</p>
                                                <div class="clock-1" data-date="<?php the_field('start_date'); ?>"></div>
                                            </div>
                                        </div>
                                        <div class="table four-el">
                                            <div class="theader">
                                                <div class="tr">
                                                    <div class="th" data-title="Pos">Pos</div>
                                                    <div class="th" data-title="Prize ($)">Prize ($)</div>
                                                    <div class="th" data-title="User">User</div>
                                                    <div class="th" data-title="Pts">Pts</div>
                                                </div>
                                            </div>
                                            <div class="tbody">
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#1</div>
                                                    <div class="td" data-title="Prize ($)">$ 2,567</div>
                                                    <div class="td" data-title="User">User 1223213438</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#2</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,890</div>
                                                    <div class="td" data-title="User">User 834539489</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#3</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,700</div>
                                                    <div class="td" data-title="User">User 45462</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#4</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,367</div>
                                                    <div class="td" data-title="User">User 25345345</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#5</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,100</div>
                                                    <div class="td" data-title="User">User 245561</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                <?php } elseif (in_category('standard-promo')) { 
                                    $platforms = get_field('platforms'); ?>
                                    <a href="<?php echo get_post_permalink(); ?>" class="promos">
                                        <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                                        <div class="description">
                                            <h2><?php echo get_the_title(); ?></h2>
                                            <div class="block-clock">
                                                <p>Time left:</p>
                                                <div class="clock-1" data-date="<?php the_field('start_date'); ?>"></div>
                                            </div>
                                            <p>Platforms: <strong>
                                                <?php if( $platforms ): 
                                                    $post = $platforms;
                                                    setup_postdata($post); ?>
                                                        <?php echo get_the_title($platforms->ID); ?>
                                                    <?php wp_reset_postdata(); ?>
                                                <?php endif; ?>
                                            </strong> </p>
                                        </div>
                                        <div class="table four-el">
                                            <div class="theader">
                                                <div class="tr">
                                                    <div class="th" data-title="Pos">Pos</div>
                                                    <div class="th" data-title="Prize ($)">Prize ($)</div>
                                                    <div class="th" data-title="User">User</div>
                                                    <div class="th" data-title="Pts">Pts</div>
                                                </div>
                                            </div>
                                            <div class="tbody">
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#1</div>
                                                    <div class="td" data-title="Prize ($)">$ 2,567</div>
                                                    <div class="td" data-title="User">User 1223213438</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#2</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,890</div>
                                                    <div class="td" data-title="User">User 834539489</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#3</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,700</div>
                                                    <div class="td" data-title="User">User 45462</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#4</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,367</div>
                                                    <div class="td" data-title="User">User 25345345</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                                <div class="tr">
                                                    <div class="td" data-title="Pos">#5</div>
                                                    <div class="td" data-title="Prize ($)">$ 1,100</div>
                                                    <div class="td" data-title="User">User 245561</div>
                                                    <div class="td" data-title="Pts">47,675</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                <?php } ?>
                            <?php endwhile; ?>
                    </div>

                    <div class="sitebar">
                        <div class="total-price">
                            <p>TOTAL PRIZES ($)<span>$20,000</span></p>
                        </div>
                        <div class="avalilable-prizes">
                            <p>&#35; AVAILABLE PRIZES <span>350</span></p>
                        </div>
                        <h3>Chase Crypto On Twitter</h3>
                        <?php echo do_shortcode('[custom-twitter-feeds]'); ?>
                        <?php if (is_active_sidebar('information_widget')) : ?>
                            <?php dynamic_sidebar('information_widget'); ?>
                        <?php endif; ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>