<?php 
/* 
* Template Name: Promos
*/ 

get_header(); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="account">
                    <div class="content">
                        <div class="slider-date">
                            <div class="item"><a href="#">January <span>2019</span></a></div>
                            <div class="item"><a href="#">February <span>2019</span></a></div>
                            <div class="item"><a href="#">March <span>2019</span></a></div>
                            <div class="item"><a href="#">April <span>2019</span></a></div>
                            <div class="item"><a href="#">May <span>2019</span></a></div>
                            <div class="item"><a href="#">June <span>2019</span></a></div>
                            <div class="item"><a href="#">July <span>2019</span></a></div>
                            <div class="item"><a href="#">August <span>2019</span></a></div>
                            <div class="item"><a href="#">September <span>2019</span></a></div>
                            <div class="item"><a href="#">October <span>2019</span></a></div>
                            <div class="item"><a href="#">November <span>2019</span></a></div>
                            <div class="item"><a href="#">December <span>2019  </span></a></div>
                        </div>
                        <a href="/promo/" class="promos">
                            <div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/banner-min.jpg);"></div>
                            <div class="description">
                                <h2>5$K CryptoChase: The World’s Biggest Crypto Promo</h2>
                                <p>Time left: 5 days + 2 hours</p>
                                <p>Platforms: <strong>Base FEX, Peppertone, 24Option MyFX Markets</strong></p>
                            </div>
                            <div class="table">
                                <div class="theader">
                                    <div class="tr">
                                        <div class="th" data-title="Pos">Pos</div>
                                        <div class="th" data-title="Prize ($)">Prize ($)</div>
                                        <div class="th" data-title="User">User</div>
                                        <div class="th" data-title="Platform">Platform</div>
                                        <div class="th" data-title="Pts">Pts</div>
                                    </div>
                                </div>
                                <div class="tbody">
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#1</div>
                                        <div class="td" data-title="Prize ($)">$ 2,567</div>
                                        <div class="td" data-title="User">User 1223213438</div>
                                        <div class="td" data-title="Platform"> <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#2</div>
                                        <div class="td" data-title="Prize ($)">$ 1,890</div>
                                        <div class="td" data-title="User">User 834539489</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#3</div>
                                        <div class="td" data-title="Prize ($)">$ 1,700</div>
                                        <div class="td" data-title="User">User 45462</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#4</div>
                                        <div class="td" data-title="Prize ($)">$ 1,367</div>
                                        <div class="td" data-title="User">User 25345345</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#5</div>
                                        <div class="td" data-title="Prize ($)">$ 1,100</div>
                                        <div class="td" data-title="User">User 245561</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="/promo/" class="promos">
                            <div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/banner-min.jpg);"></div>
                            <div class="description">
                                <h2>5$K CryptoChase: The World’s Biggest Crypto Promo</h2>
                                <p>Time left: 5 days + 2 hours</p>
                                <p>Platforms: <strong>Base FEX, Peppertone, 24Option MyFX Markets</strong></p>
                            </div>
                            <div class="table">
                                <div class="theader">
                                    <div class="tr">
                                        <div class="th" data-title="Pos">Pos</div>
                                        <div class="th" data-title="Prize ($)">Prize ($)</div>
                                        <div class="th" data-title="User">User</div>
                                        <div class="th" data-title="Platform">Platform</div>
                                        <div class="th" data-title="Pts">Pts</div>
                                    </div>
                                </div>
                                <div class="tbody">
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#1</div>
                                        <div class="td" data-title="Prize ($)">$ 2,567</div>
                                        <div class="td" data-title="User">User 1223213438</div>
                                        <div class="td" data-title="Platform"> <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#2</div>
                                        <div class="td" data-title="Prize ($)">$ 1,890</div>
                                        <div class="td" data-title="User">User 834539489</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#3</div>
                                        <div class="td" data-title="Prize ($)">$ 1,700</div>
                                        <div class="td" data-title="User">User 45462</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#4</div>
                                        <div class="td" data-title="Prize ($)">$ 1,367</div>
                                        <div class="td" data-title="User">User 25345345</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#5</div>
                                        <div class="td" data-title="Prize ($)">$ 1,100</div>
                                        <div class="td" data-title="User">User 245561</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="/promo/" class="promos">
                            <div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/banner-min.jpg);"></div>
                            <div class="description">
                                <h2>5$K CryptoChase: The World’s Biggest Crypto Promo</h2>
                                <p>Time left: 5 days + 2 hours</p>
                                <p>Platforms: <strong>Base FEX, Peppertone, 24Option MyFX Markets</strong></p>
                            </div>
                            <div class="table">
                                <div class="theader">
                                    <div class="tr">
                                        <div class="th" data-title="Pos">Pos</div>
                                        <div class="th" data-title="Prize ($)">Prize ($)</div>
                                        <div class="th" data-title="User">User</div>
                                        <div class="th" data-title="Platform">Platform</div>
                                        <div class="th" data-title="Pts">Pts</div>
                                    </div>
                                </div>
                                <div class="tbody">
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#1</div>
                                        <div class="td" data-title="Prize ($)">$ 2,567</div>
                                        <div class="td" data-title="User">User 1223213438</div>
                                        <div class="td" data-title="Platform"> <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#2</div>
                                        <div class="td" data-title="Prize ($)">$ 1,890</div>
                                        <div class="td" data-title="User">User 834539489</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#3</div>
                                        <div class="td" data-title="Prize ($)">$ 1,700</div>
                                        <div class="td" data-title="User">User 45462</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#4</div>
                                        <div class="td" data-title="Prize ($)">$ 1,367</div>
                                        <div class="td" data-title="User">User 25345345</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                    <div class="tr">
                                        <div class="td" data-title="Pos">#5</div>
                                        <div class="td" data-title="Prize ($)">$ 1,100</div>
                                        <div class="td" data-title="User">User 245561</div>
                                        <div class="td" data-title="Platform"><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></div><div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></div></div>
                                        <div class="td" data-title="Pts">47,675</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="sitebar">
                        <div class="total-price">
                            <p>TOTAL PRIZES ($)<span>$20,000</span></p>
                        </div>
                        <div class="avalilable-prizes">
                            <p>&#35; AVAILABLE PRIZES <span>350</span></p>
                        </div>
                        <h3>Chase Crypto On Twitter</h3>
                        <a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter-com-total-crypto-io-i-phone-x.jpg" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter-com-total-crypto-io-i-phone-x@2x.jpg 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/twitter-com-total-crypto-io-i-phone-x@3x.jpg 3x" alt="#"></a>
                        <h3>Join Our Top Community</h3>
                        <a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/group-5.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/group-5@2x.png 2x, <?php echo get_stylesheet_directory_uri(); ?>/img/group-5@3x.png 3x" alt="#"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>