<?php 
/* 
* Template Name: Promo
*/ 

get_header(); ?>

<section>
    <div class="promo">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="bread-crumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/listings-promos/">Promos</a></li>
                            <li><a href="#">5$K CryptoChase: The World’s Biggest Crypto Promo</a></li>
                        </ul>
                        <div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/banner.png);"></div>
                        <h2>5$K CryptoChase: The World’s Biggest Crypto Promo </h2>
                        <div class="block-clock">
                            <p>Time left:</p>
                            <div class="clock-1" data-date="Jun 28, 2019 11:00:00"></div>
                        </div>
                        <h5>Platforms:<strong> <a href="https://www.basefex.com/" target="_blank">Base FEX</a>, <a href="http://www.peppertone.com/" target="_blank">Peppertone</a>, <a href="https://www.myfxmarkets.com/en/" target="_blank">24Option MyFX Markets</a></strong></h5>
                        <a class="rules icon-arrow-bottom" href="#">rules</a>
                        <div class="rules-description">
                            <h1>Lorem ipsum dolor sit amet</h1>
                            <img class="align-left" src="<?php echo get_stylesheet_directory_uri(); ?>/img/girl.png" alt="#">
                            <h2>Lorem ipsum dolor sit amet</h2>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae eaque, harum cumque vitae tenetur minima est aliquid nemo autem fugiat id hic! Cum delectus qui non explicabo, necessitatibus expedita velit?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae necessitatibus laboriosam quod nam, iste a optio sapiente fuga quaerat, unde ad culpa, odit minus rem, doloribus hic molestias itaque dolor libero et odio iusto eligendi consequatur debitis nesciunt! Recusandae maiores, totam quidem excepturi unde facere ipsa. Impedit, numquam. Inventore, quisquam!</p>
                            <img class="align-right" src="<?php echo get_stylesheet_directory_uri(); ?>/img/group-5.png" alt="#">
                            <h3>Lorem ipsum dolor sit amet</h3>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, autem. Doloremque recusandae hic tenetur, quasi explicabo non repellendus deleniti cum veniam eos rem neque dolore nihil perspiciatis. Nesciunt, maxime itaque, voluptates reiciendis provident ipsam non quas voluptatum dolor error optio commodi ratione culpa sequi. Voluptate voluptas tenetur, quibusdam sed consequatur cumque! Placeat ut nemo laboriosam, eius illum autem officiis sequi atque velit delectus error ipsa. Soluta quia porro voluptates sequi! Obcaecati sequi velit quo itaque aperiam cupiditate sed voluptatem, est a incidunt nemo quisquam asperiores laborum accusantium, atque quis nobis.</p>
                            <img class="align-center" src="<?php echo get_stylesheet_directory_uri(); ?>/img/img.jpg" alt="#">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <ol>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                            </ol>
                            <h5>Lorem ipsum dolor sit amet</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis voluptates reprehenderit vero exercitationem debitis numquam odit accusantium, cumque perspiciatis vel fugiat deserunt suscipit quos tempore itaque soluta fugit dicta voluptatibus dolorem in, aliquid voluptatem repellat iusto saepe et! Quia aliquam doloremque ducimus quo at natus officia ullam, assumenda rem numquam temporibus nisi, totam perferendis ipsa facilis exercitationem tempora ex obcaecati amet. Saepe culpa magnam ipsam inventore explicabo, distinctio fugit impedit, eum autem doloremque, aut. Consequuntur sint veritatis hic recusandae iure quod, ipsam accusamus, eligendi earum minima tenetur laboriosam corporis, quo?</p>
                            <h6>Lorem ipsum dolor sit amet</h6>
                            <ul>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                            </ul>
                            <blockquote>
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. At delectus error accusantium! Quisquam suscipit, reprehenderit ratione debitis blanditiis voluptates et nostrum veritatis molestias numquam delectus praesentium commodi enim! Nostrum, a.</p>
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui accusantium, ab. Labore voluptatum magnam excepturi, impedit perspiciatis. Ratione, eveniet, eos.</p>
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequuntur, beatae quis magni aliquam illum, sit quam vitae est, architecto ipsam quibusdam odio. Ipsam impedit, voluptatibus recusandae saepe ut consequatur rem illum soluta rerum tenetur ab sed laudantium voluptates corrupti error atque, aspernatur pariatur voluptate doloribus quae commodi, voluptatem! Dolorum numquam iure sit nemo sunt ab nobis mollitia, fugit consectetur doloremque expedita voluptatum eius. Neque nostrum asperiores, non fuga perferendis adipisci! Quia esse officiis aut dolorem magnam omnis libero facere incidunt harum quos, soluta facilis expedita, voluptate minima, nesciunt. Perspiciatis.</p>
                            </blockquote>
                        </div>
                        <div class="table">
                            <div class="theader">
                                <div class="tr">
                                    <div class="th" data-title="Pos">Pos</div>
                                    <div class="th" data-title="Prize ($)">Prize ($)</div>
                                    <div class="th" data-title="User">User</div>
                                    <div class="th" data-title="Platform"> Platform</div>
                                    <div class="th" data-title="Pts">Pts</div>
                                </div>
                            </div>
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td" data-title="Pos">#1</div>
                                    <div class="td" data-title="Prize ($)">$ 2,567</div>
                                    <div class="td" data-title="User">User 1223213438</div>
                                    <div class="td" data-title="Platform"> <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#2</div>
                                    <div class="td" data-title="Prize ($)">$ 1,890</div>
                                    <div class="td" data-title="User">User 834539489</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#3</div>
                                    <div class="td" data-title="Prize ($)">$ 1,700</div>
                                    <div class="td" data-title="User">User 45462</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#4</div>
                                    <div class="td" data-title="Prize ($)">$ 1,367</div>
                                    <div class="td" data-title="User">User 25345345</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#5</div>
                                    <div class="td" data-title="Prize ($)">$ 1,100</div>
                                    <div class="td" data-title="User">User 245561</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#6</div>
                                    <div class="td" data-title="Prize ($)">$ 1,000</div>
                                    <div class="td" data-title="User">User 903485</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#7</div>
                                    <div class="td" data-title="Prize ($)">$ 900</div>
                                    <div class="td" data-title="User">User 9458393</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#8</div>
                                    <div class="td" data-title="Prize ($)">$ 867</div>
                                    <div class="td" data-title="User">User 34858935</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#9</div>
                                    <div class="td" data-title="Prize ($)">$ 456</div>
                                    <div class="td" data-title="User">User 439578459</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#10</div>
                                    <div class="td" data-title="Prize ($)">$ 234</div>
                                    <div class="td" data-title="User">User 4w8754759</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="reports">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="reports-update icon-arrow-bottom" href="#">Last report update</a></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>