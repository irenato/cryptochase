<?php 
/* 
* Template Name: Promo
*/ 

get_header(); ?>

<section>
    <div class="promo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="rules icon-arrow-bottom" href="#">rules</a></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="rules-description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et. Aenean eu enim justo. Vestibulum aliquam hendrerit molestie. Mauris malesuada nisi sit amet augue accumsan tincidunt. Maecenas tincidunt, velit ac porttitor pulvinar, tortor eros facilisis libero, vitae commodo nunc quam et ligula. Ut nec ipsum sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer id nisi nec nulla luctus lacinia non eu turpis. Etiam in ex imperdiet justo tincidunt egestas. Ut porttitor urna ac augue cursus tincidunt sit amet sed orci.</p>
                        </div>
                        <ul class="bread-crumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Promos</a></li>
                            <li><a href="#">5$K CryptoChase: The World’s Biggest Crypto Promo</a></li>
                        </ul>
                        <div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/banner.png);"></div>
                        <h2>5$K CryptoChase: The World’s Biggest Crypto Promo </h2>
                        <h4>Time left: 5 days + 2 hours</h4>
                        <h5>Platforms:<strong> <a href="#">Base FEX</a>, <a href="#">Peppertone</a>, <a href="#">24Option MyFX Markets</a></strong></h5>
                        <div class="table">
                            <div class="theader">
                                <div class="tr">
                                    <div class="th" data-title="Pos">Pos</div>
                                    <div class="th" data-title="Prize ($)">Prize ($)</div>
                                    <div class="th" data-title="User">User</div>
                                    <div class="th" data-title="Platform"> Platform</div>
                                    <div class="th" data-title="Pts">Pts</div>
                                </div>
                            </div>
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td" data-title="Pos">#1</div>
                                    <div class="td" data-title="Prize ($)">$ 2,567</div>
                                    <div class="td" data-title="User">User 1223213438</div>
                                    <div class="td" data-title="Platform"> <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#2</div>
                                    <div class="td" data-title="Prize ($)">$ 1,890</div>
                                    <div class="td" data-title="User">User 834539489</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#3</div>
                                    <div class="td" data-title="Prize ($)">$ 1,700</div>
                                    <div class="td" data-title="User">User 45462</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#4</div>
                                    <div class="td" data-title="Prize ($)">$ 1,367</div>
                                    <div class="td" data-title="User">User 25345345</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#5</div>
                                    <div class="td" data-title="Prize ($)">$ 1,100</div>
                                    <div class="td" data-title="User">User 245561</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#6</div>
                                    <div class="td" data-title="Prize ($)">$ 1,000</div>
                                    <div class="td" data-title="User">User 903485</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#7</div>
                                    <div class="td" data-title="Prize ($)">$ 900</div>
                                    <div class="td" data-title="User">User 9458393</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#8</div>
                                    <div class="td" data-title="Prize ($)">$ 867</div>
                                    <div class="td" data-title="User">User 34858935</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#9</div>
                                    <div class="td" data-title="Prize ($)">$ 456</div>
                                    <div class="td" data-title="User">User 439578459</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#10</div>
                                    <div class="td" data-title="Prize ($)">$ 234</div>
                                    <div class="td" data-title="User">User 4w8754759</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="reports">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="reports-update icon-arrow-bottom" href="#">Last report update</a></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul>
                        <li>
                            <a href="#">
                                <div class="img"><img src="img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>