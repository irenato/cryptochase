<?php 
/* 
* Template Name: All Promos
*/ 

get_header(); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="account">
                    <div class="content">
                        <div class="slider-date">
                            <div class="item"><a href="#">January <span>2019</span></a></div>
                            <div class="item"><a href="#">February <span>2019</span></a></div>
                            <div class="item"><a href="#">March <span>2019</span></a></div>
                            <div class="item"><a href="#">April <span>2019</span></a></div>
                            <div class="item"><a href="#">May <span>2019</span></a></div>
                            <div class="item"><a href="#">June <span>2019</span></a></div>
                            <div class="item"><a href="#">July <span>2019</span></a></div>
                            <div class="item"><a href="#">August <span>2019</span></a></div>
                            <div class="item"><a href="#">September <span>2019</span></a></div>
                            <div class="item"><a href="#">October <span>2019</span></a></div>
                            <div class="item"><a href="#">November <span>2019</span></a></div>
                            <div class="item"><a href="#">December <span>2019  </span></a></div>
                        </div>
                        <span>CryptoChase brings you the biggest and best cryptocurrency promotions, giveaways & airdrops in the world. Sign up to a CryptoChase partner platform and get rewarded for trading or gambling crypto.</span>
                        
                    </div>

                    <div class="sitebar">
                        <div class="total-price">
                            <p>TOTAL PRIZES ($)<span>$20,000</span></p>
                        </div>
                        <div class="avalilable-prizes">
                            <p>&#35; AVAILABLE PRIZES <span>350</span></p>
                        </div>
                        <h3>Chase Crypto On Twitter</h3>
                        <?php echo do_shortcode('[custom-twitter-feeds]'); ?>
                        <?php if (is_active_sidebar('information_widget')) : ?>
                            <?php dynamic_sidebar('information_widget'); ?>
                        <?php endif; ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>