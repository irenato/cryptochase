<?php 
/* 
* Template Name: Generic Promo
*/ 

get_header(); 
$attachment_image   = get_the_post_thumbnail_url(get_the_ID()); ?>

<section>
    <div class="promo">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="bread-crumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/listings-promos/">Generic Promos</a></li>
                            <li><a href="#"><?php the_title(); ?></a></li>
                        </ul>
                        <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                        <h2><?php the_title(); ?></h2>
                        <div class="timer_deals">
                            <div class="block-clock">
                                <p>Time left:</p>
                                <div class="clock-1" data-date="<?php the_field('start_date'); ?>"></div>
                            </div>
                            <?php if(have_rows('deals', get_the_ID())): ?>
                                <?php while(have_rows('deals', get_the_ID())): the_row(); 
                                    $title_deals          = get_sub_field('title_deals');
                                    $description_deals    = get_sub_field('description_deals');
                                    $rating_deals         = get_sub_field('rating_deals');
                                    $logo_deals           = get_sub_field('logo_deals');
                                    $sign_up_deals        = get_sub_field('sign_up_deals');
                                    $review_deals         = get_sub_field('review_deals'); ?>
                                    <div class="el-coin">
                                        <div class="img"><img src="<?php echo $logo_deals['url']; ?>" alt="#"></div>
                                        <div class="desc">
                                            <h6><?php echo $title_deals; ?><span class="icon-star"><?php echo $rating_deals; ?> </span></h6>
                                            <p><?php echo $description_deals; ?></p>
                                        </div>
                                        <div class="link"><a class="button" href="<?php echo $sign_up_deals; ?>">Sign Up</a><a class="review" href="<?php echo $review_deals; ?>">Review</a></div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <a class="rules icon-arrow-bottom" href="#">rules</a>
                        <div class="rules-description">
                            <?php the_field('rules'); ?>
                        </div>
                        <div class="table four-el">
                            <div class="theader">
                                <div class="tr">
                                    <div class="th" data-title="Pos">Pos</div>
                                    <div class="th" data-title="Prize ($)">Prize ($)</div>
                                    <div class="th" data-title="User">User</div>
                                    <div class="th" data-title="Pts">Pts</div>
                                </div>
                            </div>
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td" data-title="Pos">#1</div>
                                    <div class="td" data-title="Prize ($)">$ 2,567</div>
                                    <div class="td" data-title="User">User 1223213438</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#2</div>
                                    <div class="td" data-title="Prize ($)">$ 1,890</div>
                                    <div class="td" data-title="User">User 834539489</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#3</div>
                                    <div class="td" data-title="Prize ($)">$ 1,700</div>
                                    <div class="td" data-title="User">User 45462</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#4</div>
                                    <div class="td" data-title="Prize ($)">$ 1,367</div>
                                    <div class="td" data-title="User">User 25345345</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#5</div>
                                    <div class="td" data-title="Prize ($)">$ 1,100</div>
                                    <div class="td" data-title="User">User 245561</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#6</div>
                                    <div class="td" data-title="Prize ($)">$ 1,000</div>
                                    <div class="td" data-title="User">User 903485</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#7</div>
                                    <div class="td" data-title="Prize ($)">$ 900</div>
                                    <div class="td" data-title="User">User 9458393</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#8</div>
                                    <div class="td" data-title="Prize ($)">$ 867</div>
                                    <div class="td" data-title="User">User 34858935</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#9</div>
                                    <div class="td" data-title="Prize ($)">$ 456</div>
                                    <div class="td" data-title="User">User 439578459</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#10</div>
                                    <div class="td" data-title="Prize ($)">$ 234</div>
                                    <div class="td" data-title="User">User 4w8754759</div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                            </div><img class="woman" src="<?php echo get_stylesheet_directory_uri(); ?>/img/girl.png" alt="#">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="reports"> 
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p class="reports-update" href="#">Last report update 26/04/2019</p>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>