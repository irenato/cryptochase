<?php 
/* 
* Template Name: Promo
*/ 

get_header(); 
$attachment_image   = get_the_post_thumbnail_url(get_the_ID()); 
$platforms = get_field('platforms'); 

$import_prizes_structure = get_field('import_prizes_structure')['url'];
$import_daily_reports    = get_field('import_daily_reports')['url'];

$array_import = array();

$import_prizes_structure_csv = f_parse_csv($import_prizes_structure, 1000, ';');
unset($import_prizes_structure_csv[0]);

$import_daily_reports_csv = f_parse_csv($import_daily_reports, 1000, ';');
unset($import_daily_reports_csv[0]); 

$appended = array_merge(array_values($import_prizes_structure_csv), array_values($import_daily_reports_csv));  
print_r($appended); ?>

<section>
    <div class="promo">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="bread-crumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/listings-promos/">Promos</a></li>
                            <li><a href="#"><?php the_title(); ?></a></li>
                        </ul>
                        <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                        <h2><?php the_title(); ?></h2>
                        <div class="block-clock">
                            <p>Time left:</p>
                            <div class="clock-1" data-date="<?php the_field('end_date'); ?>"></div>
                        </div>
                        <h5>Platforms:<strong> 
                            <?php 
                                foreach($platforms as $platform):
                                    $get_url = get_field('url', $platform->ID);
                                    echo '<a href="' . $get_url . '" target="_blank">' . $platform->post_title . '</a>, ';
                                endforeach; 
                            ?>
                        </strong></h5>
                        <a class="rules icon-arrow-bottom" href="#">rules</a>
                        <div class="rules-description">
                            <?php the_field('rules'); ?>
                        </div>
                        <div class="table">
                            <div class="theader">
                                <div class="tr">
                                    <div class="th" data-title="Pos">Pos</div>
                                    <div class="th" data-title="Prize ($)">Prize ($)</div>
                                    <div class="th" data-title="User">User</div>
                                    <div class="th" data-title="Platform"> Platform</div>
                                    <div class="th" data-title="Pts">Pts</div>
                                </div>
                            </div>
                            <div class="tbody">
                                <?php 
                                foreach ($array_import as $arr) {
                                    print_r($arr);
                                    foreach ($arr as $ar) { 
                                        echo '
                                        <div class="tr">
                                            <div class="td" data-title="Pos">#' . $ar[1] . '</div>
                                            <div class="td" data-title="Prize ($)">$ ' . $ar[2] . '</div>
                                            <div class="td" data-title="User">' . $ar[3] . '</div>
                                            <div class="td" data-title="Platform"> <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                            <div class="td" data-title="Pts">' . $ar[4] . '</div>
                                        </div>
                                        ';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="reports">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="reports-update icon-arrow-bottom" href="#">Last report update</a></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>