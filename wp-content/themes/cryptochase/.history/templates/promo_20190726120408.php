<?php 
/* 
* Template Name: Promo
*/ 

get_header(); 
$attachment_image   = get_the_post_thumbnail_url(get_the_ID()); 
$platforms = get_field('platforms'); 

$import_prizes_structure = get_field('import_prizes_structure')['url'];

$arr_csv = parse_csv($import_prizes_structure, array("to_object" => true));
print_r($arr_csv); ?>

<section>
    <div class="promo">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="bread-crumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/listings-promos/">Promos</a></li>
                            <li><a href="#"><?php the_title(); ?></a></li>
                        </ul>
                        <div class="banner" style="background-image: url(<?php echo $attachment_image; ?>);"></div>
                        <h2><?php the_title(); ?></h2>
                        <div class="block-clock">
                            <p>Time left:</p>
                            <div class="clock-1" data-date="<?php the_field('end_date'); ?>"></div>
                        </div>
                        <h5>Platforms:<strong> 
                            <?php 
                                foreach($platforms as $platform):
                                    $get_url = get_field('url', $platform->ID);
                                    echo '<a href="' . $get_url . '" target="_blank">' . $platform->post_title . '</a>, ';
                                endforeach; 
                            ?>
                        </strong></h5>
                        <a class="rules icon-arrow-bottom" href="#">rules</a>
                        <div class="rules-description">
                            <?php the_field('rules'); ?>
                        </div>
                        <div class="table">
                            <div class="theader">
                                <div class="tr">
                                    <div class="th" data-title="Pos">Pos</div>
                                    <div class="th" data-title="Prize ($)">Prize ($)</div>
                                    <div class="th" data-title="User">User</div>
                                    <div class="th" data-title="Platform"> Platform</div>
                                    <div class="th" data-title="Pts">Pts</div>
                                </div>
                            </div>
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td" data-title="Pos">#1</div>
                                    <div class="td" data-title="Prize ($)">$ 2,567</div>
                                    <div class="td" data-title="User">User 1223213438</div>
                                    <div class="td" data-title="Platform"> <a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#2</div>
                                    <div class="td" data-title="Prize ($)">$ 1,890</div>
                                    <div class="td" data-title="User">User 834539489</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#3</div>
                                    <div class="td" data-title="Prize ($)">$ 1,700</div>
                                    <div class="td" data-title="User">User 45462</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#4</div>
                                    <div class="td" data-title="Prize ($)">$ 1,367</div>
                                    <div class="td" data-title="User">User 25345345</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#5</div>
                                    <div class="td" data-title="Prize ($)">$ 1,100</div>
                                    <div class="td" data-title="User">User 245561</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#6</div>
                                    <div class="td" data-title="Prize ($)">$ 1,000</div>
                                    <div class="td" data-title="User">User 903485</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#7</div>
                                    <div class="td" data-title="Prize ($)">$ 900</div>
                                    <div class="td" data-title="User">User 9458393</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#8</div>
                                    <div class="td" data-title="Prize ($)">$ 867</div>
                                    <div class="td" data-title="User">User 34858935</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-p.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#9</div>
                                    <div class="td" data-title="Prize ($)">$ 456</div>
                                    <div class="td" data-title="User">User 439578459</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                                <div class="tr">
                                    <div class="td" data-title="Pos">#10</div>
                                    <div class="td" data-title="Prize ($)">$ 234</div>
                                    <div class="td" data-title="User">User 4w8754759</div>
                                    <div class="td" data-title="Platform"><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-bitmap.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-crypto-ag.png" alt="#"></a><a class="img" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-mtgox.png" alt="#"></a></div>
                                    <div class="td" data-title="Pts">47,675</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="reports">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="reports-update icon-arrow-bottom" href="#">Last report update</a></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logos/logo-ardor.png" alt="#"></div>
                                <p>26/04/2019</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>